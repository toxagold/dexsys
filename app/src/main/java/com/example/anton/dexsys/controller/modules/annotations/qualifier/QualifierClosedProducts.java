package com.example.anton.dexsys.controller.modules.annotations.qualifier;

import javax.inject.Qualifier;

@Qualifier
public @interface QualifierClosedProducts {
}
