package com.example.anton.dexsys.controller.modules.components;

import com.example.anton.dexsys.controller.modules.annotations.scopes.ScopeDetailsActivity;
import com.example.anton.dexsys.controller.modules.details.DetailsNavigatorModule;
import com.example.anton.dexsys.controller.modules.details.FragmentAdapterModule;
import com.example.anton.dexsys.controller.modules.details.MapModules;
import com.example.anton.dexsys.ui.details.DetailsActivity;
import com.example.anton.dexsys.ui.details.map.MapFragment;

import dagger.Component;

@ScopeDetailsActivity
@Component(dependencies = AppComponent.class,
        modules = {DetailsNavigatorModule.class,
                FragmentAdapterModule.class,
                MapModules.class})
public interface DetailsActivityComponent {

    void inject(MapFragment fragment);
    void inject(DetailsActivity activity);
}
