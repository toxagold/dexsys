package com.example.anton.dexsys.model.data.local.map;

import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

public class Location{

    @SerializedName("latitude")
    private double latitude;
    @SerializedName("longitude")
    private double longitude;

    public Location(double latitude, double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }
}
