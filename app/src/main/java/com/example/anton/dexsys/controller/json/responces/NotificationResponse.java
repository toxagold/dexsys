package com.example.anton.dexsys.controller.json.responces;

import com.example.anton.dexsys.model.data.local.notifications.Notification;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class NotificationResponse {

    @SerializedName("resultCode")
    @Expose
    private Integer resultCode;
    @SerializedName("error")
    @Expose
    private String error;
    @SerializedName("result")
    @Expose
    private List<Notification> result = null;

    public Integer getResultCode() {
        return resultCode;
    }

    public String getError() {
        return error;
    }

    public List<Notification> getResult() {
        return result;
    }
}
