package com.example.anton.dexsys.model.converters;

import android.support.annotation.NonNull;

import com.example.anton.dexsys.controller.json.responces.NotificationResponse;
import com.example.anton.dexsys.model.data.local.notifications.Notification;

import java.util.ArrayList;
import java.util.List;

public class NotificationConverterOfResponse {

    private List<Notification> notifications = new ArrayList<>();

    @NonNull
    public List<Notification> convert(NotificationResponse response){
        notifications.addAll(response.getResult());
        return notifications;
    }
}
