package com.example.anton.dexsys.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;

import com.example.anton.dexsys.R;
import com.example.anton.dexsys.controller.modules.annotations.navigation.ScreenKeyActivity;
import com.example.anton.dexsys.controller.navigator.MainNavigator;
import com.example.anton.dexsys.controller.modules.MainNavigatorModule;
import com.example.anton.dexsys.controller.modules.annotations.navigation.ScreenKeyFragments;
import com.example.anton.dexsys.controller.modules.components.DaggerMainActivityComponent;
import com.example.anton.dexsys.controller.modules.components.MainActivityComponent;
import com.example.anton.dexsys.controller.modules.components.MainApplication;
import com.example.anton.dexsys.controller.preferences.EnumsPreferences;
import com.example.anton.dexsys.controller.preferences.Preferences;
import com.example.anton.dexsys.ui.details.DetailsActivity;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.terrakok.cicerone.NavigatorHolder;
import ru.terrakok.cicerone.Router;

public class MainActivity extends BaseActivity{

    private static final String KEY_TYPE = "type";
    private static final String KEY_MESSAGE = "message";
    private static final String KEY_SENDER = "sender";

    @BindView(R.id.main_navigation) BottomNavigationView mainNavigation;

    @Inject
    NavigatorHolder navigatorHolder;
    @Inject
    MainNavigator navigator;
    @Inject
    Router router;
    @Inject
    Preferences preferences;
    private MainActivityComponent component;

    @Override
    protected void injectComponent() {
        component = DaggerMainActivityComponent.builder()
                .mainNavigatorModule(new MainNavigatorModule(this, R.id.main_container))
                .appComponent(((MainApplication)getApplicationContext()).getAppComponent())
                .build();
        component.inject(this);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);
        ButterKnife.bind(this);

        router.newRootScreen(ScreenKeyFragments.PRODUCT_FRAGMENT);

        mainNavigation.setOnNavigationItemSelectedListener(menuItem -> {
            switch (menuItem.getItemId()){
                case R.id.navigation_products:
                    router.newRootScreen(ScreenKeyFragments.PRODUCT_FRAGMENT);
                    return true;
                case R.id.navigation_notifications:
                    router.newRootScreen(ScreenKeyFragments.NOTIFICATION_FRAGMENT);
                    return true;
                case R.id.navigation_settings:
                    router.newRootScreen(ScreenKeyFragments.SETTING_FRAGMENT);
                    return true;
            }
            return false;
        });

        if (!preferences.readBoolean(EnumsPreferences.KEY_START_INTRO_ACTIVITY)){
            router.replaceScreen(ScreenKeyActivity.INTRO_ACTIVITY);
        }

        Intent intent = getIntent();
        if (intent != null){
            String type = intent.getStringExtra(KEY_TYPE);
            if ("chat".equals(type)){
                Intent intentDetails = new Intent(this, DetailsActivity.class);
                intent.putExtra(KEY_TYPE, type);
                startActivity(intentDetails);
            }
        }
    }

    @Override
    public MainActivityComponent getComponent() {
        return component;
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
        navigatorHolder.setNavigator(navigator);
    }

    @Override
    protected void onPause() {
        navigatorHolder.removeNavigator();
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
