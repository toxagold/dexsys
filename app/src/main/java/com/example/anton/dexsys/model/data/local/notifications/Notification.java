package com.example.anton.dexsys.model.data.local.notifications;

import android.support.annotation.NonNull;

import java.util.List;

public class Notification extends BaseNotifications {

    public Notification(Integer id, @NonNull String merchant, @NonNull String category,
                         @NonNull String sum, Boolean negativeAmount, @NonNull String image,
                         @NonNull String address, Location location, @NonNull String description,
                         Integer timestamp, List<Good> goods) {
        super(id, merchant, category, sum, negativeAmount, image, address, location, description, timestamp, goods);
    }
}
