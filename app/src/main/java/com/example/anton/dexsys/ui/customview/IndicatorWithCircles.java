package com.example.anton.dexsys.ui.customview;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;

import com.example.anton.dexsys.R;

import org.jetbrains.annotations.Contract;

public class IndicatorWithCircles extends AppCompatTextView {

    private static final int DEFAULT_COUNT_CIRCLES = 4;
    private static final int DEFAULT_CIRCLE_RADIUS_DP = 14;
    private static final int DEFAULT_DISTANCE_BETWEEN_CIRCLE_DP = DEFAULT_CIRCLE_RADIUS_DP;
    private static final int DEFAULT_STROKE_THICKNESS_DP = 4;
    private static final int DEFAULT_CIRCLE_STROKE_COLOR = Color.BLACK;
    private static final int DEFAULT_CIRCLE_INSIDE_COLOR = Color.GRAY;
    private static final int DEFAULT_ACTIVE_FILL_CIRCLE = 0;

    private Paint paintStroke;
    private Paint paintInside;
    private int countCircles;
    private int circleRadius;
    private int distanceBetween;
    private int strokeThickness;
    private int strokeColor = Color.BLACK;
    private int insideColor = Color.GRAY;
    private int activeFillCircles;

    public IndicatorWithCircles(Context context) {
        this(context, null);
    }

    public IndicatorWithCircles(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public IndicatorWithCircles(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initialize(attrs);
    }

    private void initialize(@Nullable AttributeSet attrs) {
        initializeAttrs(attrs);
        initializeUI();
    }

    private void initializeAttrs(@Nullable AttributeSet attrs) {
        if (attrs != null){
            final TypedArray array = getContext().obtainStyledAttributes(attrs, R.styleable.IndicatorWithCircles);
            try {
                countCircles = array.getInteger(R.styleable.IndicatorWithCircles_count_circles,
                        DEFAULT_COUNT_CIRCLES);
                circleRadius = array.getDimensionPixelSize(R.styleable.IndicatorWithCircles_circle_radius,
                        DEFAULT_CIRCLE_RADIUS_DP);
                distanceBetween = array.getDimensionPixelSize(R.styleable.IndicatorWithCircles_distance_between_circles,
                        DEFAULT_DISTANCE_BETWEEN_CIRCLE_DP);
                strokeThickness = array.getDimensionPixelSize(R.styleable.IndicatorWithCircles_stroke_thickness,
                        DEFAULT_STROKE_THICKNESS_DP);
                strokeColor = array.getColor(R.styleable.IndicatorWithCircles_circle_stroke_color,
                        DEFAULT_CIRCLE_STROKE_COLOR);
                insideColor = array.getColor(R.styleable.IndicatorWithCircles_circle_inside_color,
                        DEFAULT_CIRCLE_INSIDE_COLOR);
                activeFillCircles = array.getInteger(R.styleable.IndicatorWithCircles_active_fill_circle,
                        DEFAULT_ACTIVE_FILL_CIRCLE);
            } catch (Throwable throwable){
                Log.d("", throwable.getMessage());
            } finally {
                array.recycle();
            }
        } else {
            Log.d("", "AttributeSet is null");
        }
    }

    private int dpToPx(int dp){
        DisplayMetrics displayMetrics = getContext().getResources().getDisplayMetrics();
        return (int) (dp * displayMetrics.density);
    }

    public void setActiveFillCircles(int activeFillCircles) {
        if (activeFillCircles >= countCircles)
            this.activeFillCircles = countCircles;
        else
            this.activeFillCircles = activeFillCircles;
        invalidate();
    }

    @Contract(pure = true)
    private int minInteger(){
        return activeFillCircles <= countCircles ? activeFillCircles : countCircles;
    }

    private void initializeUI() {
        paintStroke = new Paint();
        paintStroke.setColor(strokeColor);
        paintStroke.setStyle(Paint.Style.STROKE);
        paintStroke.setStrokeWidth(strokeThickness);
        paintStroke.setAntiAlias(true);
        paintInside = new Paint();
        paintInside.setColor(insideColor);
        paintInside.setStyle(Paint.Style.FILL);
        paintInside.setAntiAlias(true);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        drawStroke(canvas);
        drawInside(canvas);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {

        int desiredWidth = countCircles * (circleRadius + distanceBetween);
        int desiredHeight = (circleRadius * 3) * 2;

        int widthMode = MeasureSpec.getMode(widthMeasureSpec);
        int widthSize = MeasureSpec.getSize(widthMeasureSpec);
        int heightMode = MeasureSpec.getMode(heightMeasureSpec);
        int heightSize = MeasureSpec.getSize(heightMeasureSpec);

        int width;
        int height;

        if (widthMode == MeasureSpec.EXACTLY) {
            width = widthSize;
        } else if (widthMode == MeasureSpec.AT_MOST) {
            width = Math.min(desiredWidth, widthSize);
        } else {
            width = desiredWidth;
        }

        if (heightMode == MeasureSpec.EXACTLY) {
            height = heightSize;
        } else if (heightMode == MeasureSpec.AT_MOST) {
            height = Math.min(desiredHeight, heightSize);
        } else {
            height = desiredHeight;
        }
        setMeasuredDimension(width, height);
    }

    private void drawStroke(Canvas canvas){
        for (int i = 1; i <= countCircles ; i++) {
            canvas.drawCircle(distanceBetween * i + circleRadius + strokeThickness,
                    circleRadius * 3, circleRadius, paintStroke);
        }
    }

    private void drawInside(Canvas canvas){
        for (int i = 1; i <= minInteger(); i++) {
            canvas.drawCircle(distanceBetween * i + circleRadius + strokeThickness,
                    circleRadius * 3, circleRadius, paintInside);
        }
    }
}
