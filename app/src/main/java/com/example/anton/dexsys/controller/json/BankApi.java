package com.example.anton.dexsys.controller.json;

import com.example.anton.dexsys.controller.json.responces.MapResponse;
import com.example.anton.dexsys.controller.json.responces.NotificationResponse;
import com.example.anton.dexsys.controller.json.responces.ProductResponse;
import com.example.anton.dexsys.controller.modules.annotations.enums.EnumsUrl;

import io.reactivex.Observable;
import io.reactivex.Single;
import retrofit2.http.GET;

public interface BankApi {

    @GET(EnumsUrl.URL_PRODUCTS)
    Single<ApiResponse<ProductResponse>> getProductsSingle();

    @GET(EnumsUrl.URL_PRODUCTS)
    Observable<ApiResponse<ProductResponse>> getProductsObservable();

    @GET(EnumsUrl.URL_MAP)
    Observable<MapResponse> getMaps();

    @GET(EnumsUrl.URL_NOTIFICATIONS)
    Observable<NotificationResponse> getNotifications();
}
