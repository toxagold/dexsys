package com.example.anton.dexsys.ui;

public interface OnFragmentInteractionListener {
    void onFragmentInteraction(int count);
}
