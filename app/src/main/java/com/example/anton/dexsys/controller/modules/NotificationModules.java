package com.example.anton.dexsys.controller.modules;

import android.support.annotation.NonNull;

import com.example.anton.dexsys.controller.json.BankApi;
import com.example.anton.dexsys.controller.modules.annotations.scopes.ScopeNotificationFragment;
import com.example.anton.dexsys.model.converters.NotificationConverterOfResponse;
import com.example.anton.dexsys.ui.notifications.NotificationInteractor;
import com.example.anton.dexsys.ui.notifications.NotificationRepository;

import dagger.Module;
import dagger.Provides;

@Module
public class NotificationModules {

    @ScopeNotificationFragment
    @Provides
    public NotificationConverterOfResponse provideResponse(){
        return new NotificationConverterOfResponse();
    }

    @ScopeNotificationFragment
    @Provides
    NotificationInteractor provideInteractor(@NonNull BankApi bankApi,
                                             @NonNull NotificationRepository repository,
                                             @NonNull NotificationConverterOfResponse converter){
        return new NotificationInteractor(bankApi,
                repository,
                converter);
    }

    @ScopeNotificationFragment
    @Provides
    NotificationRepository provideRepository(){
        return new NotificationRepository();
    }
}
