package com.example.anton.dexsys.ui.details.map;

import android.support.annotation.NonNull;
import android.util.Log;

import com.example.anton.dexsys.R;
import com.example.anton.dexsys.model.data.local.map.Places;
import com.example.anton.dexsys.ui.base.BasePresenter;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;

import java.util.List;

import io.reactivex.disposables.Disposable;

public class MapPresenter extends BasePresenter<MapView>{

    @NonNull
    private MapInteractor interactor;
    @NonNull
    private MapRepository repository;

    public MapPresenter(@NonNull MapInteractor interactor, @NonNull MapRepository repository) {
        this.interactor = interactor;
        this.repository = repository;
    }

    public void setCameraLocation(double latitude, double longitude, GoogleMap map){
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(new LatLng(latitude, longitude))
                .zoom(15)
                .build();

        CameraUpdate cameraUpdate = CameraUpdateFactory.newCameraPosition(cameraPosition);
        map.moveCamera(cameraUpdate);
    }

    public void initState(){
        load();
    }

    private void load() {
        Disposable disposable = interactor.getBaseMap()
                .subscribe(this::onGetResult, this::onGetError);
        compositeDisposable.add(disposable);
    }

    private void onGetError(Throwable throwable) {
        Log.d("TAG", "OnGetError: " + throwable.getMessage());
    }

    private void onGetResult(List<Places> places) {
        if (repository.getPlaces().size() == 0){
            repository.savePlaces(places);
        }
    }

    public void setMarkers(@NonNull List<Places> places, GoogleMap map){

    }


    @NonNull
    public BitmapDescriptor setIconToMarker(){
        return BitmapDescriptorFactory.fromResource(R.drawable.ic_marker);
    }
}
