package com.example.anton.dexsys.model.data.local.products;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

public abstract class BaseProducts {

    @NonNull
    @SerializedName("name")
    private String name;
    @NonNull
    @SerializedName("openedDate")
    private Date openedDate;
    @SerializedName("amount")
    private Double amount;
    @NonNull
    @SerializedName("currency")
    private String currency;
    @Nullable
    @SerializedName("status")
    private String status;

    public BaseProducts(@NonNull String name, @NonNull Date openedDate,
                        Double amount, @NonNull String currency, String status) {
        this.name = name;
        this.openedDate = openedDate;
        this.amount = amount;
        this.currency = currency;
        this.status = status;
    }

    @NonNull
    public String getName() {
        return name;
    }

    @NonNull
    public Date getOpenedDate() {
        return openedDate;
    }

    public Double getAmount() {
        return amount;
    }

    @NonNull
    public String getCurrency() {
        return currency;
    }

    @Nullable
    public String getStatus() {
        return status;
    }
}
