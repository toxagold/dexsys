package com.example.anton.dexsys.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.example.anton.dexsys.controller.modules.components.MainActivityComponent;

public abstract class BaseActivity extends AppCompatActivity {

    protected MainActivityComponent component;

    protected abstract void injectComponent();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        injectComponent();
        super.onCreate(savedInstanceState);
    }

    public MainActivityComponent getComponent() {
        return component;
    }
}
