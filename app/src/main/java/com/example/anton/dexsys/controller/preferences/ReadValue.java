package com.example.anton.dexsys.controller.preferences;

import android.support.annotation.NonNull;

public interface ReadValue {

    String readString(@NonNull String key);
    Boolean readBoolean(@NonNull String key);
    Integer readInteger(@NonNull String key);
}
