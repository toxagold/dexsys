package com.example.anton.dexsys.ui.pin;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.example.anton.dexsys.R;
import com.example.anton.dexsys.controller.modules.components.MainApplication;
import com.example.anton.dexsys.ui.customview.IndicatorWithCircles;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PinActivity extends AppCompatActivity {

    @BindView(R.id.tvOne) TextView tvOne;
    @BindView(R.id.tvTwo) TextView tvTwo;
    @BindView(R.id.tvThree) TextView tvThree;
    @BindView(R.id.tvFour) TextView tvFour;
    @BindView(R.id.tvFive) TextView tvFive;
    @BindView(R.id.tvSix) TextView tvSix;
    @BindView(R.id.tvSeven) TextView tvSeven;
    @BindView(R.id.tvEight) TextView tvEight;
    @BindView(R.id.tvNine) TextView tvNine;
    @BindView(R.id.tvForgot) TextView tvForgot;
    @BindView(R.id.tvZero) TextView tvZero;
    @BindView(R.id.tvRemove) TextView tvRemove;

    @BindView(R.id.pinIndicator) IndicatorWithCircles indicator;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pin_activity);
        ButterKnife.bind(this);

    }

    public void keyPress(View view) {
        switch (view.getId()){
            case R.id.tvOne:

                break;
            case R.id.tvTwo:

                break;
            case R.id.tvThree:

                break;
            case R.id.tvFour:

                break;
            case R.id.tvFive:

                break;
            case R.id.tvSix:

                break;
            case R.id.tvSeven:

                break;
            case R.id.tvEight:

                break;
            case R.id.tvNine:

                break;
            case R.id.tvForgot:
                Log.d("TAG", "Forgot");
                break;
            case R.id.tvZero:

                break;
            case R.id.tvRemove:
                Log.d("TAG", "Remove");
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
            }

    @Override
    protected void onPause() {
                super.onPause();
    }
}
