package com.example.anton.dexsys.controller.modules.annotations.navigation;

import android.support.annotation.StringDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.SOURCE)
@StringDef({ScreenKeyFragments.PRODUCT_FRAGMENT, ScreenKeyFragments.NOTIFICATION_FRAGMENT,
ScreenKeyFragments.SETTING_FRAGMENT, ScreenKeyFragments.CARD_FRAGMENT,
        ScreenKeyFragments.NOTIFICATION_DETAILS_FRAGMENT})
public @interface ScreenKeyFragments {

    String PRODUCT_FRAGMENT = "fragment_product";
    String NOTIFICATION_FRAGMENT = "fragment_notification";
    String SETTING_FRAGMENT = "fragment_setting";
    String MAP_FRAGMENT = "map_fragment";
    String CARD_FRAGMENT = "card_fragment";
    String NOTIFICATION_DETAILS_FRAGMENT = "notification_details_fragment";
}
