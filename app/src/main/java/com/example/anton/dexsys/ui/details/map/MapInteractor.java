package com.example.anton.dexsys.ui.details.map;

import android.support.annotation.NonNull;

import com.example.anton.dexsys.controller.json.BankApi;
import com.example.anton.dexsys.model.converters.MapConverterOfResponse;
import com.example.anton.dexsys.model.data.local.map.Places;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class MapInteractor {

    @NonNull
    private BankApi bankApi;
    @NonNull
    private MapConverterOfResponse converter;
    @NonNull
    private MapRepository repository;

    @Inject
    public MapInteractor(@NonNull BankApi bankApi,
                         @NonNull MapConverterOfResponse converter,
                         @NonNull MapRepository repository) {
        this.bankApi = bankApi;
        this.converter = converter;
        this.repository = repository;
    }

    @NonNull
    public Observable<List<Places>> getBaseMap(){
        return bankApi.getMaps()
                .map(response -> converter.convert(response))
                .doOnNext(places -> repository.savePlaces(places))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }
}
