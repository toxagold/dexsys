package com.example.anton.dexsys.controller.modules.details;

import android.support.annotation.NonNull;

import com.example.anton.dexsys.controller.json.BankApi;
import com.example.anton.dexsys.model.converters.MapConverterOfResponse;
import com.example.anton.dexsys.ui.details.map.MapInteractor;
import com.example.anton.dexsys.ui.details.map.MapPresenter;
import com.example.anton.dexsys.ui.details.map.MapRepository;

import dagger.Module;
import dagger.Provides;

@Module
public class MapModules {

    @Provides
    MapConverterOfResponse provideConverter(){
        return new MapConverterOfResponse();
    }

    @Provides
    public MapInteractor provideInteractor(@NonNull BankApi bankApi,
                                           @NonNull MapConverterOfResponse converter,
                                           @NonNull MapRepository repository){
        return new MapInteractor(bankApi, converter, repository);
    }

    @Provides
    public MapPresenter providePresenter(@NonNull MapInteractor interactor,
                                         @NonNull MapRepository repository){
        return new MapPresenter(interactor, repository);
    }

    @Provides
    MapRepository provideRepository(){
        return new MapRepository();
    }
}
