package com.example.anton.dexsys.model.data.enums;

import android.support.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.SOURCE)
@IntDef({AppConst.LOCATION_UPDATE_INTERVAL_MILLIS,
        AppConst.LOCATION_FASTEST_INTERVAL_MILLIS,
        AppConst.REQUEST_CHECK_SETTINGS})
public @interface AppConst {

    int LOCATION_UPDATE_INTERVAL_MILLIS = 1000;
    int LOCATION_FASTEST_INTERVAL_MILLIS = 1001;
    int REQUEST_CHECK_SETTINGS = 1002;
}
