package com.example.anton.dexsys.model.data.local.products;

import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class Deposits extends BaseProducts {

    @SerializedName("contractNumber")
    private Long contractNumber;
    @NonNull
    @SerializedName("closingDate")
    private Date closingDate;

    public Deposits(@NonNull String name, @NonNull Date openedDate,
                    Double amount, @NonNull String currency,
                    String status, Long contractNumber, @NonNull Date closingDate) {
        super(name, openedDate, amount, currency, status);
        this.contractNumber = contractNumber;
        this.closingDate = closingDate;
    }

    public Long getContractNumber() {
        return contractNumber;
    }

    @NonNull
    public Date getClosingDate() {
        return closingDate;
    }
}
