package com.example.anton.dexsys.controller.modules.components;

import com.example.anton.dexsys.controller.modules.MainNavigatorModule;
import com.example.anton.dexsys.controller.modules.annotations.scopes.ScopeActivity;
import com.example.anton.dexsys.controller.modules.details.FragmentAdapterModule;
import com.example.anton.dexsys.ui.intro_activity.IntroActivity;
import com.example.anton.dexsys.ui.MainActivity;

import dagger.Component;

@ScopeActivity
@Component(dependencies = AppComponent.class,
        modules = {
                MainNavigatorModule.class,
                FragmentAdapterModule.class
})
public interface MainActivityComponent {

    void inject(MainActivity activity);
    void inject(IntroActivity activity);
}
