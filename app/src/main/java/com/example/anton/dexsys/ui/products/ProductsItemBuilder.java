package com.example.anton.dexsys.ui.products;

import android.support.annotation.NonNull;

import com.example.anton.dexsys.model.data.local.products.BaseProducts;
import com.example.anton.dexsys.ui.products.items.BaseProductsItem;

import java.util.List;

public interface ProductsItemBuilder {

    @NonNull
    List<BaseProductsItem> build(@NonNull List<BaseProducts> baseProducts);
}
