package com.example.anton.dexsys.controller.modules.annotations.scopes;

import javax.inject.Scope;

@Scope
public @interface ScopeDetailsActivity {
}
