package com.example.anton.dexsys.ui.intro_activity.enums;

import android.support.annotation.LayoutRes;
import android.support.annotation.StringRes;

import com.example.anton.dexsys.R;

public enum IntroEnum {
    RED(R.string.red_title, R.layout.item_viewpager_red),
    ORANGE(R.string.orange_title, R.layout.item_viewpager_orange),
    GREEN(R.string.green_title, R.layout.item_viewpager_green),
    BLUE(R.string.blue_title, R.layout.item_viewpager_blue);

    private int titleRes;
    private int layoutRes;

    IntroEnum(int titleRes, int layoutRes) {
        this.titleRes = titleRes;
        this.layoutRes = layoutRes;
    }

    @StringRes
    public int getTitleRes() {
        return titleRes;
    }

    @LayoutRes
    public int getLayoutRes() {
        return layoutRes;
    }
}
