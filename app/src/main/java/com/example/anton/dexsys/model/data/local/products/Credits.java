package com.example.anton.dexsys.model.data.local.products;

import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class Credits extends BaseProducts {

    @SerializedName("contractNumber")
    private Long contractNumber;
    @NonNull
    @SerializedName("nextPaymentDate")
    private Date nextPaymentDate;
    @SerializedName("nextPaymentAmount")
    private Double nextPaymentAmount;
    @NonNull
    @SerializedName("percent")
    private Double percent;

    public Credits(@NonNull String name, @NonNull Date openedDate,
                   Double amount, @NonNull String currency, String status,
                   Long contractNumber, @NonNull Date nextPaymentDate,
                   Double nextPaymentAmount, @NonNull Double percent) {
        super(name, openedDate, amount, currency, status);
        this.contractNumber = contractNumber;
        this.nextPaymentDate = nextPaymentDate;
        this.nextPaymentAmount = nextPaymentAmount;
        this.percent = percent;
    }

    public Long getContractNumber() {
        return contractNumber;
    }

    @NonNull
    public Date getNextPaymentDate() {
        return nextPaymentDate;
    }

    public Double getNextPaymentAmount() {
        return nextPaymentAmount;
    }

    @NonNull
    public Double getPercent() {
        return percent;
    }
}
