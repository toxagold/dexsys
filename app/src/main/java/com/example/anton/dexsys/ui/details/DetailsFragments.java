package com.example.anton.dexsys.ui.details;

import android.support.v4.app.Fragment;

public interface DetailsFragments {

    void addFragment(Fragment fragment, String title);
}
