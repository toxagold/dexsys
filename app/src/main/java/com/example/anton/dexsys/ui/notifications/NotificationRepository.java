package com.example.anton.dexsys.ui.notifications;

import com.example.anton.dexsys.model.data.local.notifications.Notification;

import java.util.ArrayList;
import java.util.List;

public class NotificationRepository {

    private List<Notification> notifications = new ArrayList<>();

    public List<Notification> getNotifications() {
        return notifications;
    }

    public void setNotifications(List<Notification> notifications) {
        this.notifications = notifications;
    }
}
