package com.example.anton.dexsys.ui.products.delegates;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.TextView;

import com.example.anton.dexsys.R;
import com.example.anton.dexsys.ui.products.ProductsAdapter;
import com.example.anton.dexsys.ui.products.ProductsItemClickListener;
import com.example.anton.dexsys.ui.products.items.BaseProductsItem;
import com.example.anton.dexsys.ui.products.items.DepositsProductsItem;
import com.hannesdorfmann.adapterdelegates3.AdapterDelegate;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DepositDelegate extends AdapterDelegate<List<BaseProductsItem>> {

    @NonNull
    private final ProductsItemClickListener listener;

    public DepositDelegate(@NonNull ProductsItemClickListener listener) {
        this.listener = listener;
    }

    @Override
    protected boolean isForViewType(@NonNull List<BaseProductsItem> items, int position) {
        return items.get(position) instanceof DepositsProductsItem;
    }

    @NonNull
    @Override
    protected RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_deposits_products, parent, false);
        return new DepositVH(view);
    }

    @Override
    protected void onBindViewHolder(@NonNull List<BaseProductsItem> items, int position, @NonNull RecyclerView.ViewHolder holder, @NonNull List<Object> payloads) {
        DepositVH depositVH = (DepositVH) holder;
        DepositsProductsItem depositItem = (DepositsProductsItem) items.get(position);
        depositVH.tvNameDeposits.setText(depositItem.getName());
        depositVH.tvOpenedDeposits.setText(depositItem.getOpenedDate().toString());
        depositVH.tvAmountDeposits.setText(depositItem.getAmount().toString());
    }

    class DepositVH extends RecyclerView.ViewHolder implements InitializeClickListener{

        @BindView(R.id.tvNameDeposits) TextView tvNameDeposits;

        @BindView(R.id.tvOpenedDeposits) TextView tvOpenedDeposits;

        @BindView(R.id.tvAmountDeposits) TextView tvAmountDeposits;

        public DepositVH(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            initListener();
        }

        @Override
        public void initListener() {
            /*
            BaseProductsItem item = (BaseProductsItem) view.getTag();
            if (item != null){
                listener.onItemClick(view);
            }
             */
            itemView.setOnClickListener(listener::onItemClick);
        }
    }
}
