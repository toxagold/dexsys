package com.example.anton.dexsys.controller.modules.components;

import com.example.anton.dexsys.controller.modules.NotificationModules;
import com.example.anton.dexsys.controller.modules.annotations.scopes.ScopeNotificationFragment;
import com.example.anton.dexsys.ui.notifications.NotificationFragment;

import dagger.Component;

@ScopeNotificationFragment
@Component(dependencies = AppComponent.class,
        modules = {
                NotificationModules.class
})
public interface NotificationFragmentComponent {

    void inject(NotificationFragment fragment);
}
