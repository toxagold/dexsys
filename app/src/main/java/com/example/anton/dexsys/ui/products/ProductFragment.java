package com.example.anton.dexsys.ui.products;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.OrientationHelper;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.anton.dexsys.R;
import com.example.anton.dexsys.controller.modules.components.MainApplication;
import com.example.anton.dexsys.ui.NewInstanceFragments;
import com.example.anton.dexsys.ui.base.BaseFragment;
import com.example.anton.dexsys.ui.products.items.BaseProductsItem;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProductFragment extends BaseFragment<ProductsPresenter>
implements NewInstanceFragments, ProductsView {

    private static final String COUNT = "count";
    @BindView(R.id.recyclerProducts)
    RecyclerView recyclerViewProducts;

    @Inject
    ProductsPresenter presenter;
    @Inject
    ProductsItemClickListener clickListener;
    ProductsAdapter adapter;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        ((MainApplication) getActivity().getApplicationContext()).getProductsComponent().inject(this);
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_product, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        initRecyclerView(view);
        presenter.initState();
    }

    private void initRecyclerView(View view) {
        recyclerViewProducts.setLayoutManager(new LinearLayoutManager(view.getContext(),
                OrientationHelper.VERTICAL, false));
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    protected ProductsPresenter getPresenter() {
        return presenter;
    }

    @Override
    public void showProducts(@NonNull List<BaseProductsItem> baseProductsItems) {
        adapter = new ProductsAdapter(baseProductsItems, clickListener);
        clickListener.setBaseProductsItems(baseProductsItems);
        clickListener.setContext(getActivity().getApplicationContext());
        recyclerViewProducts.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void showProductsEmptyList() {

    }

    @Override
    public void showProductsError() {

    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public Fragment newInstance() {
        return new ProductFragment();
    }
}
