package com.example.anton.dexsys.ui.intro_activity;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.anton.dexsys.ui.intro_activity.enums.IntroEnum;

public class IntroPagerAdapter extends PagerAdapter {

    @NonNull
    private final Context context;

    public IntroPagerAdapter(@NonNull Context context) {
        this.context = context;
    }

    @Override
    public Object instantiateItem(ViewGroup collection, int position) {
        IntroEnum value = IntroEnum.values()[position];
        LayoutInflater inflater = LayoutInflater.from(context);
        ViewGroup layout = (ViewGroup) inflater.inflate(value.getLayoutRes(), collection, false);
        collection.addView(layout);
        return layout;
    }

    @Override
    public void destroyItem(ViewGroup collection, int position, Object view) {
        collection.removeView((View) view);
    }

    @Override
    public int getCount() {
        return IntroEnum.values().length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        IntroEnum value = IntroEnum.values()[position];
        return context.getString(value.getTitleRes());
    }
}
