package com.example.anton.dexsys.ui.details.animation;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.view.View;
import android.view.animation.DecelerateInterpolator;

public class MoveAnimation{

    public static void setAnimation(View view, int fromX, int toX, int fromY, int toY){
        AnimatorSet set = new AnimatorSet();
        set.playTogether(
                ObjectAnimator.ofFloat(view, View.SCALE_X, 0.1f, 1.0f)
                        .setDuration(5000),
                ObjectAnimator.ofFloat(view, View.SCALE_Y, 0.1f, 1.0f)
                        .setDuration(5000),
                ObjectAnimator.ofFloat(view, View.TRANSLATION_X, fromX, toX)
                .setDuration(5000),
                ObjectAnimator.ofFloat(view, View.TRANSLATION_Y, fromY, toY)
                .setDuration(5000)
        );
        set.setInterpolator(new DecelerateInterpolator());
        set.start();
    }
}
