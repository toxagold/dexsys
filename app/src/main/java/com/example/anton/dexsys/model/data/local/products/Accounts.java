package com.example.anton.dexsys.model.data.local.products;

import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class Accounts extends BaseProducts {

    @SerializedName("contractNumber")
    private Long contractNumber;
    @SerializedName("percent")
    private Double percent;

    public Accounts(@NonNull String name, @NonNull Date openedDate,
                    Double amount, @NonNull String currency,
                    String status, Long contractNumber, Double percent) {
        super(name, openedDate, amount, currency, status);
        this.contractNumber = contractNumber;
        this.percent = percent;
    }

    public Long getContractNumber() {
        return contractNumber;
    }

    public Double getPercent() {
        return percent;
    }
}
