package com.example.anton.dexsys.controller.modules;

import android.content.Context;
import android.support.annotation.NonNull;

import com.example.anton.dexsys.controller.preferences.Preferences;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class PreferencesModule {

    @NonNull
    private Context context;

    public PreferencesModule(@NonNull Context context) {
        this.context = context;
    }

    @Singleton
    @Provides
    Preferences providePreferences(){
        return new Preferences(context);
    }
}
