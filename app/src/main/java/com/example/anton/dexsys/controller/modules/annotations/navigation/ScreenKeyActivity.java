package com.example.anton.dexsys.controller.modules.annotations.navigation;

import android.support.annotation.StringDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.SOURCE)
@StringDef({ScreenKeyActivity.MAIN_ACTIVITY, ScreenKeyActivity.PIN_ACTIVITY,
ScreenKeyActivity.INTRO_ACTIVITY})
public @interface ScreenKeyActivity {
    String MAIN_ACTIVITY = "main_activity";
    String PIN_ACTIVITY = "pin_activity";
    String INTRO_ACTIVITY = "intro_activity";
}
