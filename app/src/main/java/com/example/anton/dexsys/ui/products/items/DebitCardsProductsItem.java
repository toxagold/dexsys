package com.example.anton.dexsys.ui.products.items;

import android.support.annotation.NonNull;

public class DebitCardsProductsItem extends BaseProductsItem {

    @NonNull
    private String cardNumber;
    @NonNull
    private Double amount;

    public DebitCardsProductsItem(@NonNull String name, @NonNull String cardNumber, @NonNull Double amount) {
        super(name);
        this.cardNumber = cardNumber;
        this.amount = amount;
    }

    @NonNull
    public String getCardNumber() {
        return cardNumber;
    }

    @NonNull
    public Double getAmount() {
        return amount;
    }
}
