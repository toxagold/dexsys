package com.example.anton.dexsys.ui.details.map;

import com.example.anton.dexsys.model.data.local.map.Places;

import java.util.ArrayList;
import java.util.List;

public class MapRepository {

    private List<Places> places = new ArrayList<>();

    public List<Places> getPlaces() {
        return places;
    }

    public void savePlaces(List<Places> places) {
        this.places = places;
    }
}
