package com.example.anton.dexsys.ui.products.items;

import android.support.annotation.NonNull;

import java.util.Date;

public class CreditsProductsItem extends BaseProductsItem{

    @NonNull
    private Date nextPaymentDate;
    @NonNull
    private Double nextPaymentAmount;
    @NonNull
    private Double amount;

    public CreditsProductsItem(@NonNull String name, @NonNull Date nextPaymentDate,
                               @NonNull Double nextPaymentAmount, @NonNull Double amount) {
        super(name);
        this.nextPaymentDate = nextPaymentDate;
        this.nextPaymentAmount = nextPaymentAmount;
        this.amount = amount;
    }

    @NonNull
    public Date getNextPaymentDate() {
        return nextPaymentDate;
    }

    @NonNull
    public Double getNextPaymentAmount() {
        return nextPaymentAmount;
    }

    @NonNull
    public Double getAmount() {
        return amount;
    }
}
