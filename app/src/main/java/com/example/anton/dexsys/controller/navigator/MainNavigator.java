package com.example.anton.dexsys.controller.navigator;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;

import com.example.anton.dexsys.controller.modules.annotations.navigation.ScreenKeyActivity;
import com.example.anton.dexsys.controller.modules.annotations.navigation.ScreenKeyFragments;
import com.example.anton.dexsys.ui.details.notification.NotificationDetailsFragment;
import com.example.anton.dexsys.ui.intro_activity.IntroActivity;
import com.example.anton.dexsys.ui.MainActivity;
import com.example.anton.dexsys.ui.notifications.NotificationFragment;
import com.example.anton.dexsys.ui.pin.PinActivity;
import com.example.anton.dexsys.ui.products.ProductFragment;
import com.example.anton.dexsys.ui.settings.SettingFragment;

import ru.terrakok.cicerone.android.SupportAppNavigator;

public class MainNavigator extends SupportAppNavigator {

    private ProductFragment productFragment = new ProductFragment();
    private NotificationFragment notificationFragment = new NotificationFragment();
    private SettingFragment settingFragment = new SettingFragment();

    public MainNavigator(FragmentActivity activity, int containerId) {
        super(activity, containerId);
    }

    @Override
    protected Intent createActivityIntent(Context context, String screenKey, Object data) {
        switch (screenKey){
            case ScreenKeyActivity.MAIN_ACTIVITY:
                return new Intent(context, MainActivity.class);
            case ScreenKeyActivity.PIN_ACTIVITY:
                return new Intent(context, PinActivity.class);
            case ScreenKeyActivity.INTRO_ACTIVITY:
                return new Intent(context, IntroActivity.class);
                default:
                    return null;
        }
    }

    @Override
    protected Fragment createFragment(String screenKey, Object data) {
        switch (screenKey){
            case ScreenKeyFragments.PRODUCT_FRAGMENT:
                return productFragment.newInstance();
            case ScreenKeyFragments.NOTIFICATION_FRAGMENT:
                return notificationFragment.newInstance();
            case ScreenKeyFragments.SETTING_FRAGMENT:
                return settingFragment.newInstance();
                default:
                    throw new RuntimeException("Unknown screen key");
        }
    }
}
