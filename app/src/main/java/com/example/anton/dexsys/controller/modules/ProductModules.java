package com.example.anton.dexsys.controller.modules;

import android.support.annotation.NonNull;

import com.example.anton.dexsys.controller.json.BankApi;
import com.example.anton.dexsys.controller.modules.annotations.scopes.ScopeProductsFragment;
import com.example.anton.dexsys.model.converters.ProductsConverterOfResponse;
import com.example.anton.dexsys.model.repository.ProductsRepository;
import com.example.anton.dexsys.ui.products.ProductsAdapter;
import com.example.anton.dexsys.ui.products.ProductsInteractor;
import com.example.anton.dexsys.ui.products.ProductsItemBuilderImpl;
import com.example.anton.dexsys.ui.products.ProductsItemClickListener;
import com.example.anton.dexsys.ui.products.ProductsPresenter;
import com.example.anton.dexsys.ui.products.items.BaseProductsItem;

import java.util.List;

import dagger.Module;
import dagger.Provides;

@Module
public class ProductModules {

    private
    ProductsItemClickListener clickListener;

    @ScopeProductsFragment
    @Provides
    public ProductsConverterOfResponse converter(){
        return new ProductsConverterOfResponse();
    }

    @ScopeProductsFragment
    @Provides
    public ProductsInteractor interactor(@NonNull BankApi bankApi,
                                         @NonNull ProductsConverterOfResponse converter,
                                         @NonNull ProductsRepository repository){
        return new ProductsInteractor(bankApi, converter, repository);
    }

    @ScopeProductsFragment
    @Provides
    public ProductsRepository repository(){
        return new ProductsRepository();
    }

    @ScopeProductsFragment
    @Provides
    public ProductsItemBuilderImpl builder() {
        return new ProductsItemBuilderImpl();
    }

    @ScopeProductsFragment
    @Provides
    ProductsPresenter providePresenter(@NonNull ProductsInteractor interactor,
                                       @NonNull ProductsItemBuilderImpl builder,
                                       @NonNull ProductsRepository repository){
        return new ProductsPresenter(interactor, builder, repository);
    }

    @ScopeProductsFragment
    @Provides
    public ProductsAdapter adapter(@NonNull List<BaseProductsItem> baseProductsItems){
        return new ProductsAdapter(baseProductsItems, clickListener);
    }

    @ScopeProductsFragment
    @Provides
    ProductsItemClickListener provideClickListener(){
        return new ProductsItemClickListener();
    }
}
