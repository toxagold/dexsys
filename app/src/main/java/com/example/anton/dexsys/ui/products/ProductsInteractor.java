package com.example.anton.dexsys.ui.products;

import android.support.annotation.NonNull;

import com.example.anton.dexsys.model.converters.ProductsConverterOfResponse;
import com.example.anton.dexsys.model.data.local.products.BaseProducts;
import com.example.anton.dexsys.controller.json.ApiResponse;
import com.example.anton.dexsys.controller.json.BankApi;
import com.example.anton.dexsys.model.repository.ProductsRepository;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class ProductsInteractor {

    private static final String ACTIVE_PRODUCTS = "active";
    private static final String CLOSED_PRODUCTS = "closed";

    @NonNull
    private BankApi bankApi;
    @NonNull
    private ProductsConverterOfResponse converter;
    @NonNull
    private ProductsRepository repository;

    @Inject
    public ProductsInteractor(@NonNull BankApi bankApi,
                              @NonNull ProductsConverterOfResponse converter,
                              @NonNull ProductsRepository repository) {
        this.bankApi = bankApi;
        this.converter = converter;
        this.repository = repository;
    }

    @NonNull
    public Observable<List<BaseProducts>> getProducts(){
        return bankApi.getProductsObservable()
                .map(ApiResponse::getResult)
                .map(converter::convert)
                .doOnNext(repository::setAllProducts)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @NonNull
    public Observable<List<BaseProducts>> getActiveProducts(){
        return bankApi.getProductsObservable()
                .map(ApiResponse::getResult)
                .map(converter::convert)
                .flatMap(Observable::fromIterable)
                .filter(products -> {
                    assert products.getStatus() != null;
                    return (ACTIVE_PRODUCTS.equals(products.getStatus()) || products.getStatus() == null);
                })
                .toList()
                .toObservable()
                .doOnNext(repository::setActiveProducts)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @NonNull
    public Observable<List<BaseProducts>> getClosedClosedProducts(){
        return bankApi.getProductsObservable()
                .map(ApiResponse::getResult)
                .map(converter::convert)
                .flatMap(Observable::fromIterable)
                .filter(products -> {
                    assert products.getStatus() != null;
                    return CLOSED_PRODUCTS.equals(products.getStatus());
                })
                .toList()
                .toObservable()
                .doOnNext(repository::setClosedProducts)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }
}
