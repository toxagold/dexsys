package com.example.anton.dexsys.controller.modules.components;

import android.app.Application;
import android.support.annotation.NonNull;

import com.example.anton.dexsys.controller.modules.NotificationModules;
import com.example.anton.dexsys.controller.modules.NavigationModule;
import com.example.anton.dexsys.controller.modules.PreferencesModule;
import com.example.anton.dexsys.controller.modules.ProductModules;
import com.example.anton.dexsys.controller.modules.NetworkModules;

public class MainApplication extends Application {

    private AppComponent appComponent;
    private ProductsFragmentComponent productsComponent;
    private NotificationFragmentComponent notificationComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        initializeAppComponent();
        initializeProductsComponent();
        initializeNotificationComponent();
    }

    private void initializeAppComponent() {
        appComponent = DaggerAppComponent.builder()
                .networkModules(new NetworkModules())
                .navigationModule(new NavigationModule())
                .preferencesModule(new PreferencesModule(getApplicationContext()))
                .build();
    }

    private void initializeProductsComponent(){
        productsComponent = DaggerProductsFragmentComponent.builder()
                .productModules(new ProductModules())
                .appComponent(appComponent)
                .build();
    }

    private void initializeNotificationComponent(){
        notificationComponent = DaggerNotificationFragmentComponent.builder()
                .notificationModules(new NotificationModules())
                .appComponent(appComponent)
                .build();
    }

    @NonNull
    public AppComponent getAppComponent() {
        return appComponent;
    }

    @NonNull
    public ProductsFragmentComponent getProductsComponent() {
        return productsComponent;
    }

    @NonNull
    public NotificationFragmentComponent getNotificationComponent(){
        return notificationComponent;
    }
}
