package com.example.anton.dexsys.controller.preferences;

import android.support.annotation.StringDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.SOURCE)
@StringDef({EnumsPreferences.KEY_START_INTRO_ACTIVITY})
public @interface EnumsPreferences {

    String KEY_START_INTRO_ACTIVITY = "key_start";
}
