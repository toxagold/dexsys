package com.example.anton.dexsys.model.data.local.notifications;

import android.support.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Good extends BaseNotifications {

    @NonNull
    @SerializedName("name")
    @Expose
    private String name;
    @NonNull
    @SerializedName("sum")
    @Expose
    private String sum;

    public Good(Integer id, @NonNull String merchant, @NonNull String category,
                 @NonNull String sum, Boolean negativeAmount, @NonNull String image,
                 @NonNull String address, Location location, @NonNull String description,
                 Integer timestamp, List<Good> goods, @NonNull String name, @NonNull String sum1) {
        super(id, merchant, category, sum, negativeAmount, image, address, location, description, timestamp, goods);
        this.name = name;
        this.sum = sum1;
    }

    @NonNull
    public String getName() {
        return name;
    }

    @Override
    @NonNull
    public String getSum() {
        return sum;
    }
}
