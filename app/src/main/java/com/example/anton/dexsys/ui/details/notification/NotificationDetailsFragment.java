package com.example.anton.dexsys.ui.details.notification;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.anton.dexsys.R;
import com.example.anton.dexsys.ui.NewInstanceFragments;

public class NotificationDetailsFragment extends Fragment
        implements NewInstanceFragments {

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_notification_details, container, false);
    }

    @Override
    public Fragment newInstance() {
        return new NotificationDetailsFragment();
    }
}
