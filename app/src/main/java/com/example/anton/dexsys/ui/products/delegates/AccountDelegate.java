package com.example.anton.dexsys.ui.products.delegates;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.example.anton.dexsys.ui.products.items.BaseProductsItem;
import com.hannesdorfmann.adapterdelegates3.AdapterDelegate;

import java.util.List;

public class AccountDelegate extends AdapterDelegate<List<BaseProductsItem>> {

    @NonNull
    private final AdapterView.OnItemClickListener listener;

    public AccountDelegate(@NonNull AdapterView.OnItemClickListener listener) {
        this.listener = listener;
    }

    @Override
    protected boolean isForViewType(@NonNull List<BaseProductsItem> items, int position) {
        return false;
    }

    @NonNull
    @Override
    protected RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent) {
        return null;
    }

    @Override
    protected void onBindViewHolder(@NonNull List<BaseProductsItem> items, int position, @NonNull RecyclerView.ViewHolder holder, @NonNull List<Object> payloads) {

    }
}
