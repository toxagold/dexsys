package com.example.anton.dexsys.ui;

import android.support.v4.app.Fragment;

public interface NewInstanceFragments {

    Fragment newInstance();
}
