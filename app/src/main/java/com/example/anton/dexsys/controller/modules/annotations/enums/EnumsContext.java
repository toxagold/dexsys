package com.example.anton.dexsys.controller.modules.annotations.enums;

import android.support.annotation.StringDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.SOURCE)
@StringDef({EnumsContext.APPLICATION_CONTEXT, EnumsContext.ACTIVITY_CONTEXT})
public @interface EnumsContext {
    String APPLICATION_CONTEXT = "application_context";
    String ACTIVITY_CONTEXT = "activity_context";
}
