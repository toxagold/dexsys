package com.example.anton.dexsys.model.data.local.products;

import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class Payments extends BaseProducts {

    @NonNull
    @SerializedName("paymentDate")
    private Date paymentDate;
    @SerializedName("paymentAmount")
    private Double paymentAmount;

    public Payments(@NonNull String name, @NonNull Date openedDate,
                    Double amount, @NonNull String currency, String status,
                    @NonNull Date paymentDate, Double paymentAmount) {
        super(name, openedDate, amount, currency, status);
        this.paymentDate = paymentDate;
        this.paymentAmount = paymentAmount;
    }

    @NonNull
    public Date getPaymentDate() {
        return paymentDate;
    }

    public Double getPaymentAmount() {
        return paymentAmount;
    }
}
