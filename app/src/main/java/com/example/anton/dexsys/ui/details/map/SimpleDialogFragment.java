package com.example.anton.dexsys.ui.details.map;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;

import com.example.anton.dexsys.R;

public class SimpleDialogFragment extends DialogFragment {

    @Nullable
    private String title;
    @Nullable
    private String description;
    @Nullable
    private String positiveButtonName;
    @Nullable
    private String negativeButtonName;

    public static SimpleDialogFragment newInstance(@NonNull String title,
                                                   @NonNull String description,
                                                   @NonNull String positiveButtonName,
                                                   @NonNull String negativeButtonName) {
        Bundle args = new Bundle();
        args.putString("EXTRA_DIALOG_TITLE", title);
        args.putString("EXTRA_DIALOG_DESCRIPTION", description);
        args.putString("EXTRA_DIALOG_POSITIVE_BUTTON_TITLE", positiveButtonName);
        args.putString("EXTRA_DIALOG_NEGATIVE_BUTTON_TITLE", negativeButtonName);
        SimpleDialogFragment fragment = new SimpleDialogFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            title = getArguments().getString("EXTRA_DIALOG_TITLE", "");
            description = getArguments().getString("EXTRA_DIALOG_DESCRIPTION", "");
            positiveButtonName = getArguments().getString("EXTRA_DIALOG_POSITIVE_BUTTON_TITLE", "");
            negativeButtonName = getArguments().getString("EXTRA_DIALOG_NEGATIVE_BUTTON_TITLE", "");
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext(), R.style.ThemeOverlay_AppCompat_Dialog_Alert);
        builder.setTitle(title)
                .setMessage(description)
                .setCancelable(false)
                .setPositiveButton(positiveButtonName, (dialog, id) -> {
                    dialog.cancel();
                    onResult(Activity.RESULT_OK);
                })
                .setNegativeButton(negativeButtonName, (dialog, id) -> {
                    dialog.cancel();
                    onResult(Activity.RESULT_CANCELED);
                });
        return builder.create();
    }

    private void onResult(int resultCode) {
        Fragment targetFragment = getTargetFragment();
        if (targetFragment != null) {
            targetFragment.onActivityResult(getTargetRequestCode(), resultCode, null);
        }
    }

}
