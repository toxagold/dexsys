package com.example.anton.dexsys.controller.navigator;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;

import com.example.anton.dexsys.controller.modules.annotations.navigation.ScreenKeyFragments;
import com.example.anton.dexsys.ui.details.map.MapFragment;
import com.example.anton.dexsys.ui.details.notification.NotificationDetailsFragment;

import ru.terrakok.cicerone.android.SupportAppNavigator;

public class DetailsNavigator extends SupportAppNavigator {

    private MapFragment mapFragment = new MapFragment();
    private NotificationDetailsFragment detailsFragment = new NotificationDetailsFragment();

    public DetailsNavigator(FragmentActivity activity, int containerId) {
        super(activity, containerId);
    }

    @Override
    protected Intent createActivityIntent(Context context, String screenKey, Object data) {
        return null;
    }

    @Override
    protected Fragment createFragment(String screenKey, Object data) {
        switch (screenKey){
            case ScreenKeyFragments.MAP_FRAGMENT:
                return mapFragment.newInstance();
            case ScreenKeyFragments.NOTIFICATION_DETAILS_FRAGMENT:
                return detailsFragment.newInstance();
            default:
                return null;
        }
    }
}
