package com.example.anton.dexsys.ui.products;

import android.support.annotation.NonNull;
import android.util.Log;

import com.example.anton.dexsys.model.data.local.products.BaseProducts;
import com.example.anton.dexsys.model.repository.ProductsRepository;
import com.example.anton.dexsys.ui.base.BasePresenter;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.disposables.Disposable;

public class ProductsPresenter extends BasePresenter<ProductsView> {

    @NonNull
    private ProductsInteractor interactor;
    @NonNull
    private ProductsItemBuilderImpl builder;
    @NonNull
    private ProductsRepository repository;

    @Inject
    public ProductsPresenter(@NonNull ProductsInteractor interactor,
                             @NonNull ProductsItemBuilderImpl builder,
                             @NonNull ProductsRepository repository) {
        this.interactor = interactor;
        this.builder = builder;
        this.repository = repository;
    }

    public void initState(){
        load();
    }

    private void load() {
        if (repository.getActiveProducts().size() > 0){
            onGetRepository();
        } else {
            onGetRemote();
        }
    }

    private void onGetRemote(){
        Disposable disposable = interactor.getActiveProducts()
                .subscribe(this::onGetProducts, this::onGetProductsError);
        compositeDisposable.add(disposable);
    }

    private void onGetProducts(@NonNull List<BaseProducts> baseProducts) {
        getView().showProducts(builder.build(baseProducts));
    }

    private void onGetProductsError(Throwable throwable) {
        Log.d("TAG", "Method onGetError: " + throwable.getMessage());
    }

    private void onGetRepository(){
        getView().showProducts(builder.build(repository.getActiveProducts()));
    }

    public void onReloadProductsClick(){
        onGetRemote();
    }
}
