package com.example.anton.dexsys.model.data.local.products;

import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class DebitCards extends BaseProducts {

    @SerializedName("contractNumber")
    private Long contractNumber;
    @NonNull
    @SerializedName("cardNumber")
    private String cardNumber;
    @NonNull
    @SerializedName("imageUrl")
    private String imageUrl;

    public DebitCards(@NonNull String name, @NonNull Date openedDate,
                      Double amount, @NonNull String currency, String status,
                      Long contractNumber, @NonNull String cardNumber, @NonNull String imageUrl) {
        super(name, openedDate, amount, currency, status);
        this.contractNumber = contractNumber;
        this.cardNumber = cardNumber;
        this.imageUrl = imageUrl;
    }

    public Long getContractNumber() {
        return contractNumber;
    }

    @NonNull
    public String getCardNumber() {
        return cardNumber;
    }

    @NonNull
    public String getImageUrl() {
        return imageUrl;
    }
}
