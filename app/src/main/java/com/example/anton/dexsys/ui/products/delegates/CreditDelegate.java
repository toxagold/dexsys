package com.example.anton.dexsys.ui.products.delegates;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.anton.dexsys.R;
import com.example.anton.dexsys.ui.products.ProductsAdapter;
import com.example.anton.dexsys.ui.products.ProductsItemClickListener;
import com.example.anton.dexsys.ui.products.items.BaseProductsItem;
import com.example.anton.dexsys.ui.products.items.CreditsProductsItem;
import com.hannesdorfmann.adapterdelegates3.AdapterDelegate;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CreditDelegate extends AdapterDelegate<List<BaseProductsItem>> {

    @NonNull
    private final ProductsItemClickListener listener;

    public CreditDelegate(@NonNull ProductsItemClickListener listener) {
        this.listener = listener;
    }

    @Override
    protected boolean isForViewType(@NonNull List<BaseProductsItem> items, int position) {
        return items.get(position) instanceof CreditsProductsItem;
    }

    @NonNull
    @Override
    protected RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_credits_products, parent, false);
        return new CreditVH(view);
    }

    @Override
    protected void onBindViewHolder(@NonNull List<BaseProductsItem> items, int position, @NonNull RecyclerView.ViewHolder holder, @NonNull List<Object> payloads) {
        CreditVH creditVH = (CreditVH) holder;
        CreditsProductsItem creditItem = (CreditsProductsItem) items.get(position);
        creditVH.tvNameCredits.setText(creditItem.getName());
        creditVH.tvNextPaymentDateCredits.setText(creditItem.getNextPaymentDate().toString());
        creditVH.tvNextPaymentAmountCredits.setText(creditItem.getNextPaymentAmount().toString());
        creditVH.tvAmountCredits.setText(creditItem.getAmount().toString());
    }

    class CreditVH extends RecyclerView.ViewHolder implements InitializeClickListener{
        @BindView(R.id.tvNameCredits)
        TextView tvNameCredits;

        @BindView(R.id.tvNextPaymentDateCredits) TextView tvNextPaymentDateCredits;

        @BindView(R.id.tvNextPaymentAmountCredits) TextView tvNextPaymentAmountCredits;

        @BindView(R.id.tvAmountCredits) TextView tvAmountCredits;

        public CreditVH(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            initListener();
        }

        @Override
        public void initListener() {
            itemView.setOnClickListener(listener::onItemClick);
        }
    }
}
