package com.example.anton.dexsys.model.repository;

import android.support.annotation.NonNull;

import com.example.anton.dexsys.model.data.local.products.BaseProducts;
import com.example.anton.dexsys.ui.products.ProductsInteractor;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;

public class ProductsRepository {

    private List<BaseProducts> allProducts = new ArrayList<>();
    private List<BaseProducts> activeProducts = new ArrayList<>();
    private List<BaseProducts> closedProducts = new ArrayList<>();

    public List<BaseProducts> getAllProducts() {
        return allProducts;
    }

    public void setAllProducts(List<BaseProducts> allProducts) {
        this.allProducts = allProducts;
    }

    public List<BaseProducts> getActiveProducts() {
        return activeProducts;
    }

    public void setActiveProducts(List<BaseProducts> activeProducts) {
        this.activeProducts = activeProducts;
    }

    public List<BaseProducts> getClosedProducts() {
        return closedProducts;
    }

    public void setClosedProducts(List<BaseProducts> closedProducts) {
        this.closedProducts = closedProducts;
    }
}
