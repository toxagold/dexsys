package com.example.anton.dexsys.controller.modules.components;

import com.example.anton.dexsys.controller.modules.annotations.scopes.ScopeProductsFragment;
import com.example.anton.dexsys.controller.modules.ProductModules;
import com.example.anton.dexsys.ui.products.ProductFragment;

import dagger.Component;

@ScopeProductsFragment
@Component(dependencies = AppComponent.class,
        modules = {
                ProductModules.class
        })
public interface ProductsFragmentComponent {

    void inject(ProductFragment fragment);
}
