package com.example.anton.dexsys.controller.modules.contex;

import android.content.Context;

public interface ProvidesContextToModules {

    Context provideContext();
}
