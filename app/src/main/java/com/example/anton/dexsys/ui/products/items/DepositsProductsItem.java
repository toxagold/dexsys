package com.example.anton.dexsys.ui.products.items;

import android.support.annotation.NonNull;

import java.util.Date;

public class DepositsProductsItem extends BaseProductsItem {

    @NonNull
    private Date openedDate;
    @NonNull
    private Double amount;

    public DepositsProductsItem(@NonNull String name, @NonNull Date openedDate, @NonNull Double amount) {
        super(name);
        this.openedDate = openedDate;
        this.amount = amount;
    }

    @NonNull
    public Date getOpenedDate() {
        return openedDate;
    }

    @NonNull
    public Double getAmount() {
        return amount;
    }
}
