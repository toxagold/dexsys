package com.example.anton.dexsys.ui.notifications;

import com.example.anton.dexsys.ui.base.MvpView;

public interface NotificationView extends MvpView{

    void showNotification();
    void showNotificationEmptyList();
    void showNotificationError();
}
