package com.example.anton.dexsys.controller.service;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;

import com.example.anton.dexsys.R;
import com.example.anton.dexsys.ui.MainActivity;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Map;

public class DexsysFirebaseMessagingService extends FirebaseMessagingService {

    private static final String KEY_TYPE = "type";
    private static final String KEY_MESSAGE = "message";
    private static final String KEY_SENDER = "sender";
    private static final String VALUE_CHAT = "chat";

    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

        if (remoteMessage.getData().size() > 0) {
            Map<String, String> data = remoteMessage.getData();
            if (data.get(KEY_TYPE).equals(VALUE_CHAT) &&
                    data.containsKey(KEY_SENDER) &&
                    data.containsKey(KEY_MESSAGE)) {
                    showNotification(VALUE_CHAT, data.get(KEY_SENDER), data.get(KEY_MESSAGE));
                    sendLocalMessage(VALUE_CHAT);
            }
        }
    }

    private void sendLocalMessage(@NonNull String type) {
        if (type.equals(VALUE_CHAT)) {
            Intent intent = new Intent("dexsys.action.chat_message");
            LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
        }
    }

    private void showNotification(@NonNull String type,
                                  @NonNull String sender,
                                  @NonNull String message) {
        if (type.equals(VALUE_CHAT)) {
            Intent intent = new Intent(this, MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                    Intent.FLAG_ACTIVITY_SINGLE_TOP);
            intent.putExtra(KEY_TYPE, VALUE_CHAT);
            intent.putExtra(KEY_SENDER, sender);
            intent.putExtra(KEY_MESSAGE, message);

            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent,
                    PendingIntent.FLAG_UPDATE_CURRENT);

            NotificationManager notificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

            if (notificationManager == null) return;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                NotificationChannel channel = new NotificationChannel("DexsysApp",
                        "Dexsys канал",
                        NotificationManager.IMPORTANCE_HIGH);
                notificationManager.createNotificationChannel(channel);
            }
            Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            Notification notification = new NotificationCompat.Builder(this, "DexsysApp")
                    .setSmallIcon(getNotificationIcon())
                    .setAutoCancel(true)
                    .setSound(defaultSoundUri)
                    .setContentIntent(pendingIntent)
                    .setContentTitle("Новое сообщение от " + sender)
                    .setContentText(message)
                    .setDefaults(Notification.DEFAULT_ALL)
                    .setPriority(NotificationCompat.PRIORITY_HIGH)
                    .build();
            notificationManager.notify(0, notification);
        }
    }

    private int getNotificationIcon() {
        boolean useWhiteIcon = (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP);
        return useWhiteIcon ? R.drawable.ic_notification : R.mipmap.ic_launcher;
    }
}
