package com.example.anton.dexsys.model.converters;

import android.support.annotation.NonNull;

import com.example.anton.dexsys.controller.json.responces.MapResponse;
import com.example.anton.dexsys.model.data.local.map.Places;

import java.util.ArrayList;
import java.util.List;

public class MapConverterOfResponse {

    private List<Places> places = new ArrayList<>();

    @NonNull
    public List<Places> convert(@NonNull MapResponse response){
        places.addAll(response.getResult());
        return places;
    }
}
