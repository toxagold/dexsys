package com.example.anton.dexsys.ui.details.card;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import com.example.anton.dexsys.R;

import java.security.SecureRandom;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CardFragment extends Fragment {

    @BindView(R.id.webCard) WebView webView;
    @BindView(R.id.progressCard) ProgressBar progressBar;

    private static final String URL_HOMECREDIT = "https://www.homecredit.ru/cards/credit";
    private static final String URL_YANDEX = "https://yandex.ru/";
    private static final Integer PROGRESS_MAX = 100;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_card, container, false);
        ButterKnife.bind(this, view);
        progressBar.setMax(100);
        initWebView();
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    private void initWebView(){
        webView.setWebViewClient(new WebViewClient(){

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                Log.d("TAG", url);
                return true;
            }
        });
        webView.setWebChromeClient(new WebChromeClient(){
            @Override
            public void onProgressChanged(WebView view, int newProgress) {
                super.onProgressChanged(view, newProgress);
                progressBar.setProgress(newProgress);
            }
        });
        webView.loadUrl(URL_HOMECREDIT);
        webView.getSettings().setJavaScriptEnabled(true);
    }

    @Override
    public void onDestroyView() {
        webView.removeAllViews();
        webView.destroy();
        super.onDestroyView();
    }
}
