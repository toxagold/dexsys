package com.example.anton.dexsys.controller.json.responces;

import com.example.anton.dexsys.model.data.local.products.Accounts;
import com.example.anton.dexsys.model.data.local.products.CreditCards;
import com.example.anton.dexsys.model.data.local.products.Credits;
import com.example.anton.dexsys.model.data.local.products.DebitCards;
import com.example.anton.dexsys.model.data.local.products.Deposits;
import com.example.anton.dexsys.model.data.local.products.Payments;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ProductResponse {

    @SerializedName("deposits")
    private List<Deposits> deposits = null;
    @SerializedName("credits")
    private List<Credits> credits = null;
    @SerializedName("debitCards")
    private List<DebitCards> debitCards = null;
    @SerializedName("creditCards")
    private List<CreditCards> creditCards = null;
    @SerializedName("accounts")
    private List<Accounts> accounts = null;
    @SerializedName("payments")
    private List<Payments> payments = null;

    public List<Deposits> getDeposits() {
        return deposits;
    }

    public void setDeposits(List<Deposits> deposits) {
        this.deposits = deposits;
    }

    public List<Credits> getCredits() {
        return credits;
    }

    public void setCredits(List<Credits> credits) {
        this.credits = credits;
    }

    public List<DebitCards> getDebitCards() {
        return debitCards;
    }

    public void setDebitCards(List<DebitCards> debitCards) {
        this.debitCards = debitCards;
    }

    public List<CreditCards> getCreditCards() {
        return creditCards;
    }

    public void setCreditCards(List<CreditCards> creditCards) {
        this.creditCards = creditCards;
    }

    public List<Accounts> getAccounts() {
        return accounts;
    }

    public void setAccounts(List<Accounts> accounts) {
        this.accounts = accounts;
    }

    public List<Payments> getPayments() {
        return payments;
    }

    public void setPayments(List<Payments> payments) {
        this.payments = payments;
    }
}
