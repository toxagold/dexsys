package com.example.anton.dexsys.controller.modules.contex;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;

import com.example.anton.dexsys.controller.modules.annotations.qualifier.ActivityContext;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class ActivityContextModule implements ProvidesContextToModules{

    private Context context;

    public ActivityContextModule(@NonNull Activity context) {
        this.context = context;
    }

    @ActivityContext
    @Singleton
    @Provides
    @Override
    public Context provideContext() {
        return context;
    }
}
