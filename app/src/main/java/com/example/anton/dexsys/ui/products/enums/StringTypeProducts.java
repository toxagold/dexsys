package com.example.anton.dexsys.ui.products.enums;

import android.support.annotation.StringDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.SOURCE)
@StringDef({StringTypeProducts.DEPOSITS, StringTypeProducts.CREDITS,
StringTypeProducts.CARDS, StringTypeProducts.CLOSED_PRODUCTS})
public @interface StringTypeProducts {

    String DEPOSITS = "Вклады";
    String CREDITS = "Кредиты";
    String CARDS = "Карты";
    String CLOSED_PRODUCTS = "Закрытые продукты";
}
