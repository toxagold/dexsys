package com.example.anton.dexsys.ui.products.items;

import android.support.annotation.NonNull;

public class TitleProductsItem extends BaseProductsItem {

    public TitleProductsItem(@NonNull String name) {
        super(name);
    }
}
