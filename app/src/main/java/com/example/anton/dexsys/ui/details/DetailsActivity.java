package com.example.anton.dexsys.ui.details;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.util.Log;

import com.example.anton.dexsys.R;
import com.example.anton.dexsys.controller.modules.components.DaggerDetailsActivityComponent;
import com.example.anton.dexsys.controller.modules.components.DetailsActivityComponent;
import com.example.anton.dexsys.controller.modules.components.MainApplication;
import com.example.anton.dexsys.controller.modules.details.DetailsNavigatorModule;
import com.example.anton.dexsys.controller.modules.details.FragmentAdapterModule;
import com.example.anton.dexsys.controller.navigator.DetailsNavigator;
import com.example.anton.dexsys.ui.BaseActivity;
import com.example.anton.dexsys.ui.details.card.CardFragment;
import com.example.anton.dexsys.ui.details.map.MapFragment;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.terrakok.cicerone.NavigatorHolder;
import ru.terrakok.cicerone.Router;

public class DetailsActivity extends BaseActivity {

    private static final String KEY_TYPE = "type";
    private static final String KEY_MESSAGE = "message";
    private static final String KEY_SENDER = "sender";

    private static final String CARD_TITLE = "Карты";
    private static final String MAP_TITLE = "Наличные";

    @Inject
    NavigatorHolder navigatorHolder;
    @Inject
    DetailsNavigator navigator;
    @Inject
    Router router;
    @Inject
    DetailsFragmentsAdapter adapter;

    @BindView(R.id.detailsTabLayout) TabLayout tabLayout;
    @BindView(R.id.detailsViewPager) ViewPager viewPager;

    private DetailsActivityComponent component;

    @Override
    protected void injectComponent() {
        component = DaggerDetailsActivityComponent.builder()
                .detailsNavigatorModule(new DetailsNavigatorModule(this, R.id.details_container))
                .fragmentAdapterModule(new FragmentAdapterModule(this))
                .appComponent(((MainApplication)getApplicationContext()).getAppComponent())
                .build();

        component.inject(this);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.details_activity);
        ButterKnife.bind(this);

        Intent intent = getIntent();
        if (intent != null){
            String type = intent.getStringExtra(KEY_TYPE);
            Log.d("TAG", "TAG " + type);
        }

        initAdapter();
    }

    private void initAdapter(){

        adapter.addFragment(new CardFragment(), CARD_TITLE);
        adapter.addFragment(new MapFragment(), MAP_TITLE);

        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);
    }

    @Override
    protected void onResume() {
        super.onResume();
        navigatorHolder.setNavigator(navigator);
    }

    @Override
    protected void onPause() {
        navigatorHolder.removeNavigator();
        super.onPause();
    }

    public DetailsActivityComponent getDetailsComponent() {
        return component;
    }
}