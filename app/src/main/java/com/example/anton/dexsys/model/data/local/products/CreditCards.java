package com.example.anton.dexsys.model.data.local.products;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class CreditCards extends BaseProducts {

    @SerializedName("contractNumber")
    private Long contractNumber;
    @NonNull
    @SerializedName("cardNumber")
    private String cardNumber;
    @Nullable
    @SerializedName("closingDate")
    private Date closingDate;
    @NonNull
    @SerializedName("overdueDate")
    private Date overdueDate;
    @NonNull
    @SerializedName("imageUrl")
    private String imageUrl;

    public CreditCards(@NonNull String name, @NonNull Date openedDate,
                       Double amount, @NonNull String currency, String status,
                       Long contractNumber, @NonNull String cardNumber, @Nullable Date closingDate,
                       @NonNull Date overdueDate, @NonNull String imageUrl) {
        super(name, openedDate, amount, currency, status);
        this.contractNumber = contractNumber;
        this.cardNumber = cardNumber;
        this.closingDate = closingDate;
        this.overdueDate = overdueDate;
        this.imageUrl = imageUrl;
    }

    public Long getContractNumber() {
        return contractNumber;
    }

    @NonNull
    public String getCardNumber() {
        return cardNumber;
    }

    @Nullable
    public Date getClosingDate() {
        return closingDate;
    }

    @NonNull
    public Date getOverdueDate() {
        return overdueDate;
    }

    @NonNull
    public String getImageUrl() {
        return imageUrl;
    }
}
