package com.example.anton.dexsys.controller.modules.details;

import android.support.annotation.NonNull;

import com.example.anton.dexsys.controller.modules.annotations.scopes.ScopeDetailsActivity;
import com.example.anton.dexsys.ui.details.DetailsActivity;
import com.example.anton.dexsys.ui.details.DetailsFragmentsAdapter;

import dagger.Module;
import dagger.Provides;

@Module
public class FragmentAdapterModule {

    @NonNull
    DetailsActivity activity;

    public FragmentAdapterModule(@NonNull DetailsActivity activity) {
        this.activity = activity;
    }

    @ScopeDetailsActivity
    @Provides
    DetailsFragmentsAdapter provideAdapter(){
        return new DetailsFragmentsAdapter(activity.getSupportFragmentManager());
    }
}
