package com.example.anton.dexsys.ui.products;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.TextView;

import com.example.anton.dexsys.R;
import com.example.anton.dexsys.ui.products.delegates.CreditCardsDelegate;
import com.example.anton.dexsys.ui.products.delegates.CreditDelegate;
import com.example.anton.dexsys.ui.products.delegates.DebitCardsDelegate;
import com.example.anton.dexsys.ui.products.delegates.DepositDelegate;
import com.example.anton.dexsys.ui.products.delegates.TitleDelegate;
import com.example.anton.dexsys.ui.products.enums.IntTypeProducts;
import com.example.anton.dexsys.ui.products.items.BaseProductsItem;
import com.example.anton.dexsys.ui.products.items.CreditCardsProductsItem;
import com.example.anton.dexsys.ui.products.items.CreditsProductsItem;
import com.example.anton.dexsys.ui.products.items.DebitCardsProductsItem;
import com.example.anton.dexsys.ui.products.items.DepositsProductsItem;
import com.example.anton.dexsys.ui.products.items.TitleProductsItem;
import com.hannesdorfmann.adapterdelegates3.AdapterDelegatesManager;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProductsAdapter extends RecyclerView.Adapter<ViewHolder>{

    @NonNull
    private final AdapterDelegatesManager<List<BaseProductsItem>> delegatesManager;
    @NonNull
    private final List<BaseProductsItem> items;


    public ProductsAdapter(@NonNull List<BaseProductsItem> items,
                           @NonNull ProductsItemClickListener listener) {
        this.items = items;
        delegatesManager = new AdapterDelegatesManager<>();
        delegatesManager.addDelegate(new DepositDelegate(listener));
        delegatesManager.addDelegate(new CreditDelegate(listener));
        delegatesManager.addDelegate(new DebitCardsDelegate(listener));
        delegatesManager.addDelegate(new CreditCardsDelegate(listener));
        delegatesManager.addDelegate(new TitleDelegate());
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return delegatesManager.onCreateViewHolder(parent, viewType);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        delegatesManager.onBindViewHolder(items, position, holder);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void setItems(@NonNull List<BaseProductsItem> items){
        items.clear();
        items.addAll(items);
        notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) {
        return delegatesManager.getItemViewType(items, position);
    }
}
