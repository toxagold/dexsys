package com.example.anton.dexsys.controller.modules;

import android.support.annotation.IdRes;
import android.support.v4.app.FragmentActivity;

import com.example.anton.dexsys.controller.modules.annotations.scopes.ScopeActivity;
import com.example.anton.dexsys.controller.navigator.MainNavigator;

import javax.inject.Inject;

import dagger.Module;
import dagger.Provides;

@Module
public class MainNavigatorModule {

    private final FragmentActivity fragmentActivity;

    @IdRes
    private final int container;

    @Inject
    public MainNavigatorModule(FragmentActivity fragmentActivity, @IdRes int container) {
        this.fragmentActivity = fragmentActivity;
        this.container = container;
    }

    @ScopeActivity
    @Provides
    MainNavigator mainNavigator(){
        return new MainNavigator(fragmentActivity, container);
    }
}
