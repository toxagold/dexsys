package com.example.anton.dexsys.controller.preferences;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;

public class Preferences implements WriteValue, ReadValue{

    @NonNull
    private Context context;
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;

    private static final String STORAGE_NAME = "storing_setting_for_dexsys";

    public Preferences(@NonNull Context context) {
        this.context = context;
        initialization();
    }

    private void initialization() {
        preferences = context.getSharedPreferences(STORAGE_NAME, Context.MODE_PRIVATE);
        editor = preferences.edit();
        editor.apply();
    }

    @Override
    public String readString(@NonNull String key) {
        if (preferences == null)
            initialization();
        return preferences.getString(key, "");
    }

    @Override
    public Boolean readBoolean(@NonNull String key) {
        if (preferences == null)
            initialization();
        return preferences.getBoolean(key, false);
    }

    @Override
    public Integer readInteger(@NonNull String key) {
        if (preferences == null)
            initialization();
        return preferences.getInt(key, 0);
    }

    @Override
    public void write(@NonNull String key, @NonNull String value) {
        if (preferences == null)
            initialization();
        editor.putString(key, value);
        editor.apply();
    }

    @Override
    public void write(@NonNull String key, @NonNull Boolean value) {
        if (preferences == null)
            initialization();
        editor.putBoolean(key, value);
        editor.apply();
    }

    @Override
    public void write(@NonNull String key, @NonNull Integer value) {
        if (preferences == null)
            initialization();
        editor.putInt(key, value);
        editor.apply();
    }
}
