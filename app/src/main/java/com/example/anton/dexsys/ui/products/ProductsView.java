package com.example.anton.dexsys.ui.products;

import android.support.annotation.NonNull;

import com.example.anton.dexsys.ui.base.MvpView;
import com.example.anton.dexsys.ui.products.items.BaseProductsItem;

import java.util.List;

public interface ProductsView extends MvpView{

    void showProducts(@NonNull List<BaseProductsItem> baseProductsItems);
    void showProductsEmptyList();
    void showProductsError();
}
