package com.example.anton.dexsys.model.data.local.map;

import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

public class BaseMap {

    @SerializedName("id")
    private int id;
    @NonNull
    @SerializedName("category")
    private String category;
    @NonNull
    @SerializedName("image")
    private String image;
    @NonNull
    @SerializedName("address")
    private String address;
    @NonNull
    @SerializedName("description")
    private String description;

    public BaseMap(int id, @NonNull String category,
                   @NonNull String image, @NonNull String address,
                   @NonNull String description) {
        this.id = id;
        this.category = category;
        this.image = image;
        this.address = address;
        this.description = description;
    }

    public int getId() {
        return id;
    }

    @NonNull
    public String getCategory() {
        return category;
    }

    @NonNull
    public String getImage() {
        return image;
    }

    @NonNull
    public String getAddress() {
        return address;
    }

    @NonNull
    public String getDescription() {
        return description;
    }
}
