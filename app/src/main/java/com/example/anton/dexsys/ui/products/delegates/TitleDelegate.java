package com.example.anton.dexsys.ui.products.delegates;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.anton.dexsys.R;
import com.example.anton.dexsys.ui.products.ProductsAdapter;
import com.example.anton.dexsys.ui.products.items.BaseProductsItem;
import com.example.anton.dexsys.ui.products.items.TitleProductsItem;
import com.hannesdorfmann.adapterdelegates3.AdapterDelegate;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TitleDelegate extends AdapterDelegate<List<BaseProductsItem>> {
    @Override
    protected boolean isForViewType(@NonNull List<BaseProductsItem> items, int position) {
        return items.get(position) instanceof TitleProductsItem;
    }

    @NonNull
    @Override
    protected RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_title_products, parent, false);
        return new TitleVH(view);
    }

    @Override
    protected void onBindViewHolder(@NonNull List<BaseProductsItem> items, int position, @NonNull RecyclerView.ViewHolder holder, @NonNull List<Object> payloads) {
        TitleVH titleVH = (TitleVH) holder;
        TitleProductsItem titleItem = (TitleProductsItem) items.get(position);
        titleVH.tvTitle.setText(titleItem.getName());
    }

    class TitleVH extends RecyclerView.ViewHolder{
        @BindView(R.id.tvTitle)
        TextView tvTitle;

        public TitleVH(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
