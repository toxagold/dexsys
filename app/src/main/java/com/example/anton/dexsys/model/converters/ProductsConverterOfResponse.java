package com.example.anton.dexsys.model.converters;

import android.support.annotation.NonNull;

import com.example.anton.dexsys.model.data.local.products.BaseProducts;
import com.example.anton.dexsys.controller.json.responces.ProductResponse;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class ProductsConverterOfResponse {

    @Inject
    public ProductsConverterOfResponse() {
    }

    private List<BaseProducts> baseProducts = new ArrayList<>();

    @NonNull
    public List<BaseProducts> convert(@NonNull ProductResponse response){
        baseProducts.addAll(response.getDeposits());
        baseProducts.addAll(response.getCredits());
        baseProducts.addAll(response.getDebitCards());
        baseProducts.addAll(response.getCreditCards());
        baseProducts.addAll(response.getAccounts());
        return baseProducts;
    }
}
