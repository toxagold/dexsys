package com.example.anton.dexsys.model.data.local.notifications;

import android.support.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public abstract class BaseNotifications {

    @SerializedName("id")
    @Expose
    private Integer id;
    @NonNull
    @SerializedName("merchant")
    @Expose
    private String merchant;
    @NonNull
    @SerializedName("category")
    @Expose
    private String category;
    @NonNull
    @SerializedName("sum")
    @Expose
    private String sum;
    @SerializedName("negativeAmount")
    @Expose
    private Boolean negativeAmount;
    @NonNull
    @SerializedName("image")
    @Expose
    private String image;
    @NonNull
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("location")
    @Expose
    private Location location;
    @NonNull
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("timestamp")
    @Expose
    private Integer timestamp;
    @SerializedName("goods")
    @Expose
    private List<Good> goods = null;

    public BaseNotifications(Integer id, @NonNull String merchant, @NonNull String category,
                             @NonNull String sum, Boolean negativeAmount, @NonNull String image,
                             @NonNull String address, Location location, @NonNull String description,
                             Integer timestamp, List<Good> goods) {
        this.id = id;
        this.merchant = merchant;
        this.category = category;
        this.sum = sum;
        this.negativeAmount = negativeAmount;
        this.image = image;
        this.address = address;
        this.location = location;
        this.description = description;
        this.timestamp = timestamp;
        this.goods = goods;
    }

    public Integer getId() {
        return id;
    }

    @NonNull
    public String getMerchant() {
        return merchant;
    }

    @NonNull
    public String getCategory() {
        return category;
    }

    @NonNull
    public String getSum() {
        return sum;
    }

    public Boolean getNegativeAmount() {
        return negativeAmount;
    }

    @NonNull
    public String getImage() {
        return image;
    }

    @NonNull
    public String getAddress() {
        return address;
    }

    public Location getLocation() {
        return location;
    }

    @NonNull
    public String getDescription() {
        return description;
    }

    public Integer getTimestamp() {
        return timestamp;
    }

    public List<Good> getGoods() {
        return goods;
    }
}
