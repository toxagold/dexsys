package com.example.anton.dexsys.ui.products.enums;

import android.support.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.SOURCE)
@IntDef({IntTypeProducts.TITLE,
        IntTypeProducts.DEPOSIT,
        IntTypeProducts.CREDIT,
        IntTypeProducts.DEBIT_CARD,
        IntTypeProducts.CREDIT_CARD,
        IntTypeProducts.ACCOUNT})
public @interface IntTypeProducts {
    int TITLE = 0;
    int DEPOSIT = 1;
    int CREDIT = 2;
    int DEBIT_CARD = 3;
    int CREDIT_CARD = 4;
    int ACCOUNT = 5;
}
