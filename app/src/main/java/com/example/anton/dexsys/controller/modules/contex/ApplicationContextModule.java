package com.example.anton.dexsys.controller.modules.contex;

import android.content.Context;
import android.support.annotation.NonNull;

import com.example.anton.dexsys.controller.modules.annotations.qualifier.ApplicationContext;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class ApplicationContextModule implements ProvidesContextToModules{

    private Context context;

    public ApplicationContextModule(@NonNull Context context) {
        this.context = context;
    }

    @ApplicationContext
    @Singleton
    @Provides
    @Override
    public Context provideContext() {
        return context;
    }
}
