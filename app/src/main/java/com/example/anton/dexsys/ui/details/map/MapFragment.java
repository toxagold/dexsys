package com.example.anton.dexsys.ui.details.map;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.anton.dexsys.R;
import com.example.anton.dexsys.model.data.enums.AppConst;
import com.example.anton.dexsys.model.data.local.map.Places;
import com.example.anton.dexsys.ui.details.DetailsActivity;
import com.example.anton.dexsys.ui.NewInstanceFragments;
import com.example.anton.dexsys.ui.details.animation.MoveAnimation;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.Task;
import com.squareup.picasso.Picasso;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.RuntimePermissions;

@RuntimePermissions
public class MapFragment extends Fragment implements OnMapReadyCallback,
        NewInstanceFragments, OnMarkerClickListener {

    @Nullable
    private GoogleMap map;
    @Nullable
    private LocationRequest locationRequest;
    @Nullable
    private FusedLocationProviderClient locationClient;
    @Nullable
    private LocationCallback locationCallback;
    private BottomSheetBehavior bottomSheetBehavior;
    private static final int REQUEST_PERMISSION = 1;
    private static final int REQUEST_CODE = 2;
    private static final String AUDIO_PERMISSION_FRAGMENT = "audio";

    @BindView(R.id.bottom_sheet) RelativeLayout layout_bottom;
    @BindView(R.id.btnMap) Button buttonMap;
    @BindView(R.id.btnShowMarkers) Button buttonMarkers;
    @BindView(R.id.btnCloseBottomSheet) Button buttonClose;
    @BindView(R.id.tvCategoryBottomSheet) TextView category;
    @BindView(R.id.tvAddressBottomSheet) TextView address;
    @BindView(R.id.tvDescriptionBottomSheet) TextView descriptor;
    @BindView(R.id.imgCircleBottomSheet) CircleImageView image;

    @Inject
    MapPresenter presenter;
    @Inject
    MapRepository repository;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        locationClient = LocationServices.getFusedLocationProviderClient(getActivity().getApplicationContext());
        ((DetailsActivity)getActivity()).getDetailsComponent().inject(this);
        presenter.initState();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_map, container, false);
        initMapFragment();
        return view;
    }

    private void initMapFragment() {
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        bottomSheetBehavior = BottomSheetBehavior.from(layout_bottom);
        MoveAnimation.setAnimation(buttonMap, -100, 0, -100, 0);
        MoveAnimation.setAnimation(buttonMarkers, 100, 0, -100, 0);
    }

    @OnClick(R.id.btnMap)
    public void getLocation(){
        MapFragmentPermissionsDispatcher.methodWithPermissionCheck(this);
    }

    @NeedsPermission({Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION})
    public void method(){
        checkLocationSettings();
        if (ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

            createLocationCallback();

            requestLocationUpdates();

        } else {
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, REQUEST_PERMISSION);
        }
    }

    void checkLocationSettings() {
        locationRequest = createLocationRequest();
        LocationSettingsRequest settingsRequest = createSettingsRequest(locationRequest);
        SettingsClient settingsClient = LocationServices.getSettingsClient(getContext());
        Task<LocationSettingsResponse> task = settingsClient.checkLocationSettings(settingsRequest);
        addSettingsRequestListeners(task);
    }

    @NonNull
    private LocationRequest createLocationRequest() {
        return LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(AppConst.LOCATION_UPDATE_INTERVAL_MILLIS)
                .setFastestInterval(AppConst.LOCATION_FASTEST_INTERVAL_MILLIS);
    }

    @NonNull
    private LocationSettingsRequest createSettingsRequest(@NonNull LocationRequest locationRequest) {
        return new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest)
                .setAlwaysShow(true)
                .build();
    }

    private void addSettingsRequestListeners(@NonNull Task<LocationSettingsResponse> task) {
        task.addOnSuccessListener(locationSettingsResponse -> requestLocationUpdates());
        task.addOnFailureListener(e -> {
            int statusCode = ((ApiException) e).getStatusCode();
            if (statusCode == CommonStatusCodes.RESOLUTION_REQUIRED) {
                startSettingsResolution(e);
            }
        });
    }

    private void startSettingsResolution(@NonNull Exception e) {
        try {
            ResolvableApiException resolvable = (ResolvableApiException) e;
            resolvable.startResolutionForResult(getActivity(), AppConst.REQUEST_CHECK_SETTINGS);
        } catch (IntentSender.SendIntentException sendEx) {
        }
    }

    public void createLocationCallback() {
        locationClient = LocationServices.getFusedLocationProviderClient(getActivity());
        locationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(@NonNull LocationResult locationResult) {
                super.onLocationResult(locationResult);
                Location location = locationResult.getLastLocation();
                presenter.setCameraLocation(location.getLatitude(),
                        location.getLongitude(),
                        map);
                Log.d("TAG", String.valueOf(location.getLatitude() + " " + location.getLongitude()));
                removeLocationUpdates();
            }
        };
    }

    @SuppressLint("MissingPermission")
    void requestLocationUpdates() {
        if (map != null) {
            map.setMyLocationEnabled(true);
        }
        locationClient.requestLocationUpdates(LocationRequest.create(), locationCallback, Looper.myLooper());
    }

    @OnClick(R.id.btnShowMarkers)
    public void showMarkers(){
        SimpleDialogFragment dialogFragment = SimpleDialogFragment.newInstance("title",
                "description",
                "ok",
                "cancel");
        dialogFragment.setTargetFragment(this, REQUEST_CODE);
        dialogFragment.show(getFragmentManager(), AUDIO_PERMISSION_FRAGMENT);
    }

    @OnClick(R.id.btnCloseBottomSheet)
    public void close(){
        closeBottomSheet();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        initMap(googleMap);
        // Обращение к презентеру для получения данных для карты
        //presenter.onMapReady();
    }

    private void initMap(GoogleMap googleMap) {
        map = googleMap;
        map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        map.setIndoorEnabled(false);
        map.getUiSettings().setMyLocationButtonEnabled(false);
        map.getUiSettings().setCompassEnabled(false);
    }

    private void initMarkers(GoogleMap googleMap){
        Marker marker;

        List<Places> places = repository.getPlaces();

        for (int i = 0; i < places.size() - 1; i++) {
            marker = googleMap.addMarker(new MarkerOptions()
                    .position(new LatLng(places.get(i).getLocation().getLatitude(),
                            places.get(i).getLocation().getLongitude()))
                    .title(places.get(i).getCategory()));
            marker.setTag(i);
        }

        googleMap.setOnMarkerClickListener(this);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                initMarkers(map);
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Log.d("TAG", "click cancel");
            }
        }
    }

    private void initBottomSheet() {
        bottomSheetBehavior.setHideable(false);
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        initBottomSheetStateListener(bottomSheetBehavior);
    }

    private void closeBottomSheet() {
        bottomSheetBehavior.setHideable(true);
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
    }

    private void initBottomSheetStateListener(@NonNull BottomSheetBehavior bottomSheetBehavior) {
        bottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {

            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
            }
        });
    }

    @Override
    public Fragment newInstance() {
        return new MapFragment();
    }

    @Override
    public void onStop() {
        removeLocationUpdates();
        super.onStop();
    }

    private void removeLocationUpdates() {
        if (locationClient != null && locationCallback != null) {
            locationClient.removeLocationUpdates(locationCallback);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        MapFragmentPermissionsDispatcher.onRequestPermissionsResult(this, requestCode, grantResults);
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        category.setText(repository.getPlaces().get(Integer.parseInt(marker.getTag().toString())).getCategory());
        address.setText(repository.getPlaces().get(Integer.parseInt(marker.getTag().toString())).getAddress());
        descriptor.setText(repository.getPlaces().get(Integer.parseInt(marker.getTag().toString())).getDescription());
        Picasso.get().load(repository.getPlaces().get(Integer.parseInt(marker.getTag().toString())).getImage()).into(image);
        initBottomSheet();
        return false;
    }
}
