package com.example.anton.dexsys.ui.base;

public interface MvpView {

    void showLoading();
    void hideLoading();
}
