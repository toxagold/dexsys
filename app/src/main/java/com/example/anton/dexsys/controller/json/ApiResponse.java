package com.example.anton.dexsys.controller.json;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ApiResponse<T> {

    @SerializedName("resultCode")
    private int resultCode;
    @SerializedName("error")
    private String error;
    @SerializedName("result")
    private T result;

    public int getResultCode() {
        return resultCode;
    }

    public String getError() {
        return error;
    }

    public T getResult() {
        return result;
    }

}
