package com.example.anton.dexsys.ui.notifications;

import android.support.annotation.NonNull;

import com.example.anton.dexsys.controller.json.BankApi;
import com.example.anton.dexsys.model.converters.NotificationConverterOfResponse;

import javax.inject.Inject;

public class NotificationInteractor {

    @NonNull
    private BankApi bankApi;
    @NonNull
    private NotificationRepository repository;
    @NonNull
    private NotificationConverterOfResponse converter;

    @Inject
    public NotificationInteractor(@NonNull BankApi bankApi,
                                  @NonNull NotificationRepository repository,
                                  @NonNull NotificationConverterOfResponse converter) {
        this.bankApi = bankApi;
        this.repository = repository;
        this.converter = converter;
    }
}
