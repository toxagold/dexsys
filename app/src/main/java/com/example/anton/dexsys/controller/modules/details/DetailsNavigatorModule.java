package com.example.anton.dexsys.controller.modules.details;

import android.support.annotation.IdRes;
import android.support.v4.app.FragmentActivity;

import com.example.anton.dexsys.controller.modules.annotations.scopes.ScopeDetailsActivity;
import com.example.anton.dexsys.controller.navigator.DetailsNavigator;

import dagger.Module;
import dagger.Provides;

@Module
public class DetailsNavigatorModule {

    private final FragmentActivity fragment;

    @IdRes
    private final int container;

    public DetailsNavigatorModule(FragmentActivity fragment, @IdRes int container) {
        this.fragment = fragment;
        this.container = container;
    }

    @ScopeDetailsActivity
    @Provides
    DetailsNavigator provideNavigator(){
        return new DetailsNavigator(fragment, container);
    }
}
