package com.example.anton.dexsys.ui.products;

import android.content.Context;
import android.content.Intent;
import android.view.View;

import com.example.anton.dexsys.ui.details.DetailsActivity;
import com.example.anton.dexsys.ui.products.items.BaseProductsItem;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import ru.terrakok.cicerone.Router;

public class ProductsItemClickListener{

    @Inject
    Router router;

    private Context context;
    private List<BaseProductsItem> baseProductsItems = new ArrayList<>();


    public void onItemClick(View view){
        Intent intent = new Intent(context, DetailsActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public void setBaseProductsItems(List<BaseProductsItem> baseProductsItems) {
        this.baseProductsItems = baseProductsItems;
    }
}
