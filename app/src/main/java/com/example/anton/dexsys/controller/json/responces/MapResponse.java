package com.example.anton.dexsys.controller.json.responces;

import com.example.anton.dexsys.model.data.local.map.Places;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MapResponse {

    @SerializedName("resultCode")
    @Expose
    private Integer resultCode;
    @SerializedName("error")
    @Expose
    private String error;
    @SerializedName("result")
    @Expose
    private List<Places> result = null;

    public Integer getResultCode() {
        return resultCode;
    }

    public void setResultCode(Integer resultCode) {
        this.resultCode = resultCode;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public List<Places> getResult() {
        return result;
    }

    public void setResult(List<Places> result) {
        this.result = result;
    }
}
