package com.example.anton.dexsys.controller.modules.annotations.enums;

import android.support.annotation.StringDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.SOURCE)
@StringDef({EnumsUrl.BASE_URL, EnumsUrl.URL_PRODUCTS, EnumsUrl.URL_NOTIFICATIONS})
public @interface EnumsUrl {

    String BASE_URL = "http://www.mocky.io/";
    String URL_PRODUCTS = "v2/5b21590730000091265c7459";
    String URL_NOTIFICATIONS = "v2/5b4b3d3b2f000083001e0e9d";
    String URL_MAP = "v2/5b5e0638320000df011cfaa1";
}
