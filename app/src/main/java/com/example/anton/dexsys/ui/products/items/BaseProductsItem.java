package com.example.anton.dexsys.ui.products.items;

import android.support.annotation.NonNull;

public abstract class BaseProductsItem {

    @NonNull
    private String name;


    public BaseProductsItem(@NonNull String name) {
        this.name = name;
    }

    @NonNull
    public String getName() {
        return name;
    }
}
