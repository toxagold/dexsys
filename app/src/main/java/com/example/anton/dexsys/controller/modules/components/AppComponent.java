package com.example.anton.dexsys.controller.modules.components;

import com.example.anton.dexsys.controller.json.BankApi;
import com.example.anton.dexsys.controller.modules.NavigationModule;
import com.example.anton.dexsys.controller.modules.PreferencesModule;
import com.example.anton.dexsys.controller.modules.NetworkModules;
import com.example.anton.dexsys.controller.preferences.Preferences;

import javax.inject.Singleton;

import dagger.Component;
import ru.terrakok.cicerone.NavigatorHolder;
import ru.terrakok.cicerone.Router;

@Singleton
@Component(modules = {
        NetworkModules.class,
        NavigationModule.class,
        PreferencesModule.class
})
public interface AppComponent {
    NavigatorHolder provideNavigatorHolder();
    Router provideRouter();
    BankApi provideBankApi();
    Preferences providePreferences();
}
