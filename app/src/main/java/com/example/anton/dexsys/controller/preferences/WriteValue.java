package com.example.anton.dexsys.controller.preferences;

import android.support.annotation.NonNull;

public interface WriteValue {

    void write(@NonNull String key, @NonNull String value);
    void write(@NonNull String key, @NonNull Boolean value);
    void write(@NonNull String key, @NonNull Integer value);
}
