package com.example.anton.dexsys.ui;

import android.support.v4.app.Fragment;

public interface OnChangeFragmentListener {
    void changeFragment(Fragment fragment);
    void clearFragment();
}
