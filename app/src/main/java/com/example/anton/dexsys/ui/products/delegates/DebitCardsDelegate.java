package com.example.anton.dexsys.ui.products.delegates;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.anton.dexsys.R;
import com.example.anton.dexsys.ui.products.ProductsAdapter;
import com.example.anton.dexsys.ui.products.ProductsItemClickListener;
import com.example.anton.dexsys.ui.products.items.BaseProductsItem;
import com.example.anton.dexsys.ui.products.items.DebitCardsProductsItem;
import com.hannesdorfmann.adapterdelegates3.AdapterDelegate;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DebitCardsDelegate extends AdapterDelegate<List<BaseProductsItem>> {

    @NonNull
    private final ProductsItemClickListener listener;

    public DebitCardsDelegate(@NonNull ProductsItemClickListener listener) {
        this.listener = listener;
    }

    @Override
    protected boolean isForViewType(@NonNull List<BaseProductsItem> items, int position) {
        return items.get(position) instanceof DebitCardsProductsItem;
    }

    @NonNull
    @Override
    protected RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_debitcards_products, parent, false);
        return new DebitCardsVH(view);
    }

    @Override
    protected void onBindViewHolder(@NonNull List<BaseProductsItem> items, int position, @NonNull RecyclerView.ViewHolder holder, @NonNull List<Object> payloads) {
        DebitCardsVH debitCardVH = (DebitCardsVH) holder;
        DebitCardsProductsItem cardItem = (DebitCardsProductsItem) items.get(position);
        debitCardVH.tvDebitCardsNumber.setText(cardItem.getCardNumber());
        debitCardVH.tvDebitCardsName.setText(cardItem.getName());
        debitCardVH.tvDebitCardsAmount.setText(cardItem.getAmount().toString());
    }

    class DebitCardsVH extends RecyclerView.ViewHolder implements InitializeClickListener{
        @BindView(R.id.tvDebitCardsName)
        TextView tvDebitCardsName;

        @BindView(R.id.tvDebitCardsNumber) TextView tvDebitCardsNumber;

        @BindView(R.id.tvDebitCardsAmount) TextView tvDebitCardsAmount;

        public DebitCardsVH(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            initListener();
        }

        @Override
        public void initListener() {
            itemView.setOnClickListener(listener::onItemClick);
        }
    }
}
