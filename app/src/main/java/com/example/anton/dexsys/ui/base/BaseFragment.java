package com.example.anton.dexsys.ui.base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

public abstract class BaseFragment<P extends BasePresenter> extends Fragment{

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getPresenter().attachView((MvpView) this);
    }

    @Override
    public void onDestroyView() {
        getPresenter().detachView();
        super.onDestroyView();
    }

    protected abstract P getPresenter();
}
