package com.example.anton.dexsys.ui.base;

import android.support.annotation.NonNull;
import android.util.Log;

import com.example.anton.dexsys.ui.base.MvpView;

import io.reactivex.disposables.CompositeDisposable;

public abstract class BasePresenter<V extends MvpView>{

    @NonNull
    protected final CompositeDisposable compositeDisposable = new CompositeDisposable();
    private V view;

    public void attachView(@NonNull V view){
        this.view = view;
    }

    public void detachView(){
        view = null;
        if (!compositeDisposable.isDisposed()){
            compositeDisposable.dispose();
        }
    }

    protected boolean isViewAttached(){
        return view != null;
    }

    protected V getView(){
        return view;
    }
}
