package com.example.anton.dexsys.ui.products.delegates;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.anton.dexsys.R;
import com.example.anton.dexsys.ui.products.ProductsAdapter;
import com.example.anton.dexsys.ui.products.ProductsItemClickListener;
import com.example.anton.dexsys.ui.products.items.BaseProductsItem;
import com.example.anton.dexsys.ui.products.items.CreditCardsProductsItem;
import com.hannesdorfmann.adapterdelegates3.AdapterDelegate;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CreditCardsDelegate extends AdapterDelegate<List<BaseProductsItem>> {

    @NonNull
    private final ProductsItemClickListener listener;

    public CreditCardsDelegate(@NonNull ProductsItemClickListener listener) {
        this.listener = listener;
    }

    @Override
    protected boolean isForViewType(@NonNull List<BaseProductsItem> items, int position) {
        return items.get(position) instanceof  CreditCardsProductsItem;
    }

    @NonNull
    @Override
    protected RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_creditcards_products, parent, false);
        return new CreditCardsVH(view);
    }

    @Override
    protected void onBindViewHolder(@NonNull List<BaseProductsItem> items, int position, @NonNull RecyclerView.ViewHolder holder, @NonNull List<Object> payloads) {
        CreditCardsVH creditCardVH = (CreditCardsVH) holder;
        CreditCardsProductsItem cardItem = (CreditCardsProductsItem) items.get(position);
        creditCardVH.tvCreditCardsNumber.setText(cardItem.getCardNumber());
        creditCardVH.tvCreditCardsName.setText(cardItem.getName());
        creditCardVH.tvCreditCardsAmount.setText(cardItem.getAmount().toString());
    }

    class CreditCardsVH extends RecyclerView.ViewHolder implements InitializeClickListener{
        @BindView(R.id.tvCreditCardsName)
        TextView tvCreditCardsName;

        @BindView(R.id.tvCreditCardsNumber) TextView tvCreditCardsNumber;

        @BindView(R.id.tvCreditCardsAmount) TextView tvCreditCardsAmount;

        public CreditCardsVH(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            initListener();
        }

        @Override
        public void initListener() {
            itemView.setOnClickListener(listener::onItemClick);
        }
    }
}
