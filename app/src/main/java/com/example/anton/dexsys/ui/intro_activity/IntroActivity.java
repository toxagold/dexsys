package com.example.anton.dexsys.ui.intro_activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.example.anton.dexsys.R;
import com.example.anton.dexsys.controller.modules.annotations.navigation.ScreenKeyActivity;
import com.example.anton.dexsys.controller.modules.components.DaggerMainActivityComponent;
import com.example.anton.dexsys.controller.modules.components.MainActivityComponent;
import com.example.anton.dexsys.controller.modules.components.MainApplication;
import com.example.anton.dexsys.controller.navigator.MainNavigator;
import com.example.anton.dexsys.controller.modules.MainNavigatorModule;
import com.example.anton.dexsys.controller.preferences.EnumsPreferences;
import com.example.anton.dexsys.controller.preferences.Preferences;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ru.terrakok.cicerone.NavigatorHolder;
import ru.terrakok.cicerone.Router;

public class IntroActivity extends AppCompatActivity implements ViewPager.OnPageChangeListener {

    @Inject
    NavigatorHolder navigatorHolder;
    @Inject
    MainNavigator navigator;
    @Inject
    Router router;
    @Inject
    Preferences preferences;

    @BindView(R.id.viewPager) ViewPager viewPager;
    @BindView(R.id.btnSkip) Button buttonSkip;
    @BindView(R.id.btnNext) Button buttonNext;
    @BindView(R.id.tab_layout) TabLayout tabLayout;
    MainActivityComponent component;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.intro_activity);
        ButterKnife.bind(this);

        component = DaggerMainActivityComponent.builder()
                .mainNavigatorModule(new MainNavigatorModule(this, R.id.main_container))
                .appComponent(((MainApplication)getApplicationContext()).getAppComponent())
                .build();
        component.inject(this);

        preferences.write(EnumsPreferences.KEY_START_INTRO_ACTIVITY, true);

        initViewPager();

        viewPager.addOnPageChangeListener(this);
    }

    private void initViewPager(){
        PagerAdapter adapter = new IntroPagerAdapter(getApplicationContext());
        tabLayout.setupWithViewPager(viewPager);
        viewPager.setAdapter(adapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        navigatorHolder.setNavigator(navigator);
    }

    @Override
    protected void onPause() {
        navigatorHolder.removeNavigator();
        super.onPause();
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        if (position == 3)
            buttonSkip.setVisibility(View.INVISIBLE);
        else
            buttonSkip.setVisibility(View.VISIBLE);
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @OnClick(R.id.btnSkip)
    public void skip(){
        viewPager.setCurrentItem(viewPager.getAdapter().getCount() - 1);

    }

    @OnClick(R.id.btnNext)
    public void next(){
        int item = getItem(1);
        if (item > viewPager.getAdapter().getCount() - 1)
            router.replaceScreen(ScreenKeyActivity.MAIN_ACTIVITY);
        else
            viewPager.setCurrentItem(item);

    }

    private int getItem(int i){
        return viewPager.getCurrentItem() + i;
    }
}
