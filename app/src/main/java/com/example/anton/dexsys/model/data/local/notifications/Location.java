package com.example.anton.dexsys.model.data.local.notifications;

import android.support.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Location extends BaseNotifications {

    @NonNull
    @SerializedName("latitude")
    @Expose
    private String latitude;
    @NonNull
    @SerializedName("longitude")
    @Expose
    private String longitude;

    public Location(Integer id, @NonNull String merchant, @NonNull String category,
                    @NonNull String sum, Boolean negativeAmount, @NonNull String image,
                    @NonNull String address, Location location, @NonNull String description,
                    Integer timestamp, List<Good> goods, @NonNull String latitude,
                    @NonNull String longitude) {
        super(id, merchant, category, sum, negativeAmount, image, address, location, description, timestamp, goods);
        this.latitude = latitude;
        this.longitude = longitude;
    }

    @NonNull
    public String getLatitude() {
        return latitude;
    }

    @NonNull
    public String getLongitude() {
        return longitude;
    }
}
