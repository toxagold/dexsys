package com.example.anton.dexsys.ui.products;

import android.support.annotation.NonNull;

import com.example.anton.dexsys.model.data.local.products.BaseProducts;
import com.example.anton.dexsys.model.data.local.products.Credits;
import com.example.anton.dexsys.model.data.local.products.DebitCards;
import com.example.anton.dexsys.model.data.local.products.Deposits;
import com.example.anton.dexsys.ui.products.enums.StringTypeProducts;
import com.example.anton.dexsys.ui.products.items.BaseProductsItem;
import com.example.anton.dexsys.ui.products.items.CreditCardsProductsItem;
import com.example.anton.dexsys.ui.products.items.CreditsProductsItem;
import com.example.anton.dexsys.ui.products.items.DebitCardsProductsItem;
import com.example.anton.dexsys.ui.products.items.DepositsProductsItem;
import com.example.anton.dexsys.ui.products.items.TitleProductsItem;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class ProductsItemBuilderImpl implements ProductsItemBuilder {

    private List<BaseProductsItem> baseProductsItems = new ArrayList<>();
    private boolean isTitleCardsAdded = false;

    @Inject
    public ProductsItemBuilderImpl() {
    }

    @NonNull
    @Override
    public List<BaseProductsItem> build(@NonNull List<BaseProducts> baseProducts) {
        cleanList();
        setFalseValue();
        buildDeposits(baseProducts);
        buildCredits(baseProducts);
        buildDebitCards(baseProducts);
        buildCreditCards(baseProducts);
        return baseProductsItems;
    }

    private void cleanList(){
        if (baseProductsItems.size() > 0)
            baseProductsItems.clear();
    }

    private void setFalseValue(){
        isTitleCardsAdded = false;
    }

    private void buildDeposits(List<BaseProducts> baseProducts){
        boolean isTitleAdded = false;
        for (BaseProducts baseProduct: baseProducts) {
            if (baseProduct instanceof Deposits){
                if (!isTitleAdded){
                    baseProductsItems.add(new TitleProductsItem(StringTypeProducts.DEPOSITS));
                    isTitleAdded = true;
                }
                baseProductsItems.add(new DepositsProductsItem(baseProduct.getName(),
                        baseProduct.getOpenedDate(),
                        baseProduct.getAmount()));
            }
        }
    }

    private void buildCredits(List<BaseProducts> baseProducts){
        boolean isTitleAdded = false;
        for (BaseProducts baseProduct: baseProducts) {
            if (baseProduct instanceof Credits){
                if (!isTitleAdded){
                    baseProductsItems.add(new TitleProductsItem(StringTypeProducts.CREDITS));
                    isTitleAdded = true;
                }
                baseProductsItems.add(new CreditsProductsItem(baseProduct.getName(),
                        ((Credits) baseProduct).getNextPaymentDate(),
                        ((Credits) baseProduct).getNextPaymentAmount(),
                        baseProduct.getAmount()));
            }
        }
    }

    private void buildDebitCards(List<BaseProducts> baseProducts){
        for (BaseProducts baseProduct: baseProducts) {
            if (baseProduct instanceof DebitCards){
                if (!isTitleCardsAdded){
                    baseProductsItems.add(new TitleProductsItem(StringTypeProducts.CARDS));
                    isTitleCardsAdded = true;
                }
                baseProductsItems.add(new DebitCardsProductsItem(baseProduct.getName(),
                        ((DebitCards) baseProduct).getCardNumber(),
                        baseProduct.getAmount()));
            }
        }
    }

    private void buildCreditCards(List<BaseProducts> baseProducts){
        for (BaseProducts baseProduct: baseProducts) {
            if (baseProduct instanceof DebitCards){
                if (!isTitleCardsAdded){
                    baseProductsItems.add(new TitleProductsItem(StringTypeProducts.CARDS));
                    isTitleCardsAdded = true;
                }
                baseProductsItems.add(new CreditCardsProductsItem(baseProduct.getName(),
                        ((DebitCards) baseProduct).getCardNumber(),
                        baseProduct.getAmount()));
            }
        }
    }
}
